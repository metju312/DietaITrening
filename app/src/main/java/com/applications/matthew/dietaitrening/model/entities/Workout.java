package com.applications.matthew.dietaitrening.model.entities;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Workout extends RealmObject {

    @PrimaryKey
    private Long id;
    private String name;
    private Integer time;
    private Date startDate;
    private boolean finished;
    private RealmList<Exercise> exercises;

    public Workout() {
    }

    public Workout(Long id, String name, Integer time, Date startDate, boolean finished, RealmList<Exercise> exercises) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.startDate = startDate;
        this.finished = finished;
        this.exercises = exercises;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public RealmList<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(RealmList<Exercise> exercises) {
        this.exercises = exercises;
    }
}
