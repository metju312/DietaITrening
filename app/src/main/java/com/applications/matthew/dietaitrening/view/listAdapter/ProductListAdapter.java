package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.services.ProductService;
import com.applications.matthew.dietaitrening.view.activity.ProductPreviewActivity;

import io.realm.RealmResults;

public class ProductListAdapter extends AbstractListAdapter<Product, ProductPreviewActivity> {

    public ProductListAdapter(LayoutInflater inflater, RealmResults<Product> objects) {
        super(ProductPreviewActivity.class, inflater, objects, false);
    }

    @Override
    protected String getPrimaryTextViewText(Product product) {
        return product.getName();
    }

    @Override
    protected String getSecondaryTextViewText(Product product) {
        return product.getCalorific().toString();
    }

    @Override
    protected int getXmlLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected int getPrimaryTextViewId() {
        return R.id.itemPrimaryTextView;
    }

    @Override
    protected int getSecondaryTextViewId() {
        return R.id.itemSecondaryTextView;
    }

    @Override
    protected int getPopupMenuId() {
        return R.menu.popup_menu;
    }

    @Override
    protected int getListViewLayoutId() {
        return R.id.itemListViewLayout;
    }

    @Override
    public long getDatabaseItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected void delete(long id) {
        ProductService productService = new ProductService();
        productService.deleteById(id);
    }
}