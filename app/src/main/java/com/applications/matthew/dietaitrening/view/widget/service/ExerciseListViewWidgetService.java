package com.applications.matthew.dietaitrening.view.widget.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.utils.Format;
import com.applications.matthew.dietaitrening.view.widget.ExerciseWidgetProvider;
import com.applications.matthew.dietaitrening.view.widget.transferObject.ExerciseTransferObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmResults;

public class ExerciseListViewWidgetService extends RemoteViewsService {

    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListViewRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}

class ListViewRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context mContext;

    private List<ExerciseTransferObject> transferObjectList;

    public ListViewRemoteViewsFactory(Context context, Intent intent) {
        mContext = context;
    }

    public void onCreate() {
        onDataSetChanged();
    }

    public RemoteViews getViewAt(int position) {
        ExerciseTransferObject object = transferObjectList.get(position);
        if(object.getSecondaryText()==null){
            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_exercise_date_item);

            rv.setTextViewText(R.id.exerciseWidgetDateItem, object.getPrimaryText());

            Bundle extras = new Bundle();
            extras.putInt(ExerciseWidgetProvider.EXTRA_ITEM, position);
            return rv;
        } else {
            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_workout_item);

            rv.setTextViewText(R.id.exerciseWidgetItem, object.getPrimaryText());
            rv.setTextViewText(R.id.exerciseTimeWidgetItem, object.getSecondaryText());

            Bundle extras = new Bundle();
            extras.putInt(ExerciseWidgetProvider.EXTRA_ITEM, position);

            Intent fillInIntent = new Intent();
            fillInIntent.putExtra("actualItemId", object.getId());
            fillInIntent.putExtras(extras);

            rv.setOnClickFillInIntent(R.id.exerciseWidgetLayout, fillInIntent);
            return rv;
        }
    }

    public int getCount() {
        return transferObjectList.size();
    }

    public void onDataSetChanged() {
        transferObjectList = new ArrayList<>();

        WorkoutService workoutService = new WorkoutService();
        RealmResults<Workout> workouts = workoutService.findAllWithStartDate(Calendar.getInstance());
        ExerciseService exerciseService = new ExerciseService();
        for (Workout workout : workouts) {
            transferObjectList.add(new ExerciseTransferObject(null, workout.getName(), null));
            RealmResults<Exercise> exercises = exerciseService.findAllWithWorkoutOrderByOrder(workout);
            for (Exercise exercise : exercises) {
                transferObjectList.add(new ExerciseTransferObject(workout.getId(), exercise.getName(),exercise.getTime().toString()));
            }
        }

    }

    public int getViewTypeCount() {
        return 2;
    }

    public long getItemId(int position) {
        return position;
    }

    public void onDestroy() {
        //getRecords().clear();
    }

    public boolean hasStableIds() {
        return true;
    }

    public RemoteViews getLoadingView() {
        return null;
    }
}