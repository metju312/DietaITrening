package com.applications.matthew.dietaitrening.view.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.User;
import com.applications.matthew.dietaitrening.model.services.UserService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class CalculatorBmrActivity extends BackActivity {
    TextView bmrTextView;
    EditText bodyMassEditText;
    EditText growthEditText;
    EditText ageEditText;
    Spinner genderSpinner;
    Spinner physicalActivitySpinner;

    boolean isAlgoritmMale = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_bmr_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.bmr_index_calculator));
        customizeComponents();
        setValuesFromCurrentUser();
    }

    private void setValuesFromCurrentUser() {
        UserService userService = new UserService();
        User user = userService.findCurrentUser();
        bodyMassEditText.setText(user.getWeight().toString());
        growthEditText.setText(user.getGrowth().toString());
        ageEditText.setText(String.valueOf(userService.getUserAge(user)));

    }

    private void customizeComponents() {
        bmrTextView = (TextView) findViewById(R.id.bmrTextView);

        bodyMassEditText = (EditText) findViewById(R.id.bodyMassEditText);
        bodyMassEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tryToCalculateBmr();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        growthEditText = (EditText) findViewById(R.id.growthBodyText);
        growthEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tryToCalculateBmr();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        ageEditText = (EditText) findViewById(R.id.age);
        ageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tryToCalculateBmr();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        genderSpinner = (Spinner) findViewById(R.id.gender_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.gender_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(adapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        physicalActivitySpinner = (Spinner) findViewById(R.id.physical_activity_spinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.physical_activity_array, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        physicalActivitySpinner.setAdapter(adapter2);
        physicalActivitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void tryToCalculateBmr() {
        String bodyMassText = bodyMassEditText.getText().toString();
        String growthMassText = growthEditText.getText().toString();
        Integer age = Integer.getInteger(ageEditText.getText().toString());

        if (bodyMassText.length() >= 2 && growthMassText.length() >= 3) {
            try{
                calculateBmi(Integer.parseInt(bodyMassEditText.getText().toString()), Integer.parseInt(growthEditText.getText().toString()));
            }catch (Exception e){
                Toast.makeText(this,"Wrong numbers",Toast.LENGTH_SHORT);
            }
        } else {
            bmrTextView.setText("0.0");
            bmrTextView.setTextColor(Color.GREEN);
        }
    }


    private void calculateBmi(int bodyMass, int growth) {
        Double bmi = bodyMass/(mFromCm(growth)*mFromCm(growth));
        if(bmi < 18.5 || bmi >= 25){
            bmrTextView.setTextColor(Color.RED);
        }else{
            bmrTextView.setTextColor(Color.GREEN);
        }
        bmrTextView.setText(String.format("%.2f", bmi));
    }

    private double mFromCm(int cm){
        return (double)cm/100;
    }
}
