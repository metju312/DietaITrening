package com.applications.matthew.dietaitrening.view.application;

import android.app.Application;

import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Nutrients;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.entities.Survey;
import com.applications.matthew.dietaitrening.model.entities.User;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.model.services.ProductService;
import com.applications.matthew.dietaitrening.model.services.SetService;
import com.applications.matthew.dietaitrening.model.services.SurveyService;
import com.applications.matthew.dietaitrening.model.services.UserService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmQuery;

public class BaseApplication extends Application {
    private static BaseApplication singleton;

    private RealmConfiguration config;

    public static BaseApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        Realm.init(this);
        config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        if(!checkIfWorkoutExists(1L)){
            try {
                addTestData();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        logAllItems();
    }

    private void logAllItems() {
        ProductService productService = new ProductService();
        productService.logItems();
        MealService mealService = new MealService();
        mealService.logItems();
    }

    public boolean checkIfWorkoutExists(Long id){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Workout> query = realm.where(Workout.class)
                .equalTo("id", id);

        return query.count() != 0;
    }

    private void addTestData() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH");
        Date date = dateFormat.parse("09/01/2017 11");
        Date date1 = dateFormat.parse("10/01/2017 12");
        Date date2 = dateFormat.parse("11/01/2017 22");
        Date date3 = dateFormat.parse("12/01/2017 13");
        Date date4 = dateFormat.parse("13/01/2017 14");
        Date date5 = dateFormat.parse("14/01/2017 15");
        Date date6 = dateFormat.parse("15/01/2017 16");
        Date date7 = dateFormat.parse("16/01/2017 17");
        Date date8 = dateFormat.parse("17/01/2017 18");

        Date birthdayDate = dateFormat.parse("07/03/1994 10");
        User user = addUser("Matthew", 77.5, 70.5, 184, 0, 76, 79, birthdayDate, date, "");

        Set set1 = addSet(2,4, 30.5,40.5d);
        Set set2 = addSet(2,5, 32.5,42.5d);
        Set set3 = addSet(2,6, 33.5,43.5d);
        Set set4 = addSet(2,7, 34.5,44.5d);
        Set set5 = addSet(2,8, 35.5,45.5d);
        RealmList<Set> sets1 = new RealmList<>(set1, set2, set3);
        RealmList<Set> sets2 = new RealmList<>(set4, set5);
        Exercise push1 = addExercise("Wypychanie sztangi na ławce prostej", 0, date, 1, sets1);
        Exercise push2 = addExercise("Podnoszenie hantli siedząc", 0, date, 2, sets2);
        Exercise push3 = addExercise("Tricepsy na poręczach", 0, date, 3, sets1);
        Exercise push4 = addExercise("Przysiady", 0, date, 4, sets2);
        Exercise push5 = addExercise("Stawanie na palcach stóp", 0, date, 5, sets1);
        Exercise pull1 = addExercise("Podciąganie na drążku", 0, date, 1, sets2);
        Exercise pull2 = addExercise("Podnoszenie hantli leżąc na brzuchu", 0, date, 2, sets1);
        Exercise pull3 = addExercise("Podnoszenie hantli stojąc", 0, date, 3, sets2);
        Exercise pull4 = addExercise("Wypychanie nogami siedząc", 0, date, 4, sets1);
        Exercise pull5 = addExercise("Spiny na mięśnie brzucha", 0, date, 5, sets2);
        Exercise bieg1 = addExercise("Bieg jednostajny", 0, date, 1, sets1);
        Exercise bieg2 = addExercise("Bieg interwałowy", 0, date, 1, sets2);


        RealmList<Exercise> exercises1 = new RealmList<>(push1, push2, push3, push4, push5);
        addWorkout("Trening push1", 60, date, true, exercises1);
        RealmList<Exercise> exercises2 = new RealmList<>(pull1, pull2, pull3, pull4, pull5);
        addWorkout("Trening pull2", 62, date, true, exercises2);
        RealmList<Exercise> exercises3 = new RealmList<>(bieg1);
        addWorkout("Trening biegowy3", 62, date1, true, exercises3);
        RealmList<Exercise> exercises4 = new RealmList<>(bieg2);
        addWorkout("Trening biegowy - interwałowy4", 62, date1, true, exercises4);
        addWorkout("Trening pull5", 62, date2, false, null);
        addWorkout("Trening pull6", 62, date2, false, null);
        addWorkout("Trening pull7", 62, date3, false, null);
        addWorkout("Trening pull8", 62, date3, false, null);
        addWorkout("Trening pull9", 62, date4, false, null);
        addWorkout("Trening pull10", 62, date4, false, null);
        addWorkout("Trening pull11", 62, date5, false, null);
        addWorkout("Trening pull12", 62, date5, false, null);
        addWorkout("Trening pull13", 62, date6, false, null);
        addWorkout("Trening pull14", 62, date6, false, null);
        addWorkout("Trening pull15", 62, date7, false, null);
        addWorkout("Trening pull16", 62, date7, false, null);
        addWorkout("Trening pull17", 62, date8, false, null);
        addWorkout("Trening pull18", 62, date8, false, null);

        Product product1 = addProduct("Płatki owsiane", 100, null);
        Product product2 = addProduct("Mleko", 55, null);
        Product product3 = addProduct("Twaróg", 35, null);
        Product product4 = addProduct("Jajko", 25, null);
        Product product5 = addProduct("Chleb", 15, null);
        Product product6 = addProduct("Szynka", 25, null);
        RealmList<Product> products1 = new RealmList<>(product1, product2);
        addMeal("Płatki owsiane z mlekiem", "Dodaj płatki do mleka",15, 1, date, products1);
        RealmList<Product> products2 = new RealmList<>(product2, product3, product4);
        addMeal("Naleśniki z serem", "Dodaj mleko do sera", 45, 2, date, products2);
        RealmList<Product> products3 = new RealmList<>(product5, product6);
        addMeal("Kanapki z szynką","Na chleb połóż szynkę", 15, 3, date1,products3);

        addSurvey(70d,date1);
        addSurvey(72d,date2);
        addSurvey(73d,date3);
        addSurvey(74d,date4);
        addSurvey(75d,date5);
        addSurvey(72d,date6);
        addSurvey(74d,date7);
        addSurvey(87d,date8);
    }

    private Survey addSurvey(Double weight, Date date) {
        SurveyService surveyService = new SurveyService();
        return surveyService.createSurvey(weight, date);
    }

    private User addUser(String name, Double weight, Double weightGoal, Integer growth, Integer caloricGoal, Integer lowestWeight, Integer highestWeight, Date birthdayDate, Date joinDate, String picturePath) {
        UserService userService = new UserService();
        return userService.createUser(name, weight, weightGoal, growth, caloricGoal, lowestWeight, highestWeight, birthdayDate, joinDate, picturePath);
    }

    private Set addSet(Integer numberOfRepeatsToDo, Integer numberOfRepeatsDone, Double weightToDo, Double weightDone) {
        SetService setService = new SetService();
        return setService.createSet(numberOfRepeatsToDo, numberOfRepeatsDone, weightToDo, weightDone);
    }

    private Exercise addExercise(String name, Integer time, Date date, Integer order, RealmList<Set> sets) {
        ExerciseService exerciseService = new ExerciseService();
        return exerciseService.createExercise(name, time, date, order, sets);
    }

    private Product addProduct(String name, Integer calorific, RealmList<Nutrients> nutrientses) {
        ProductService productService = new ProductService();
        return productService.createProduct(name, calorific, nutrientses);
    }

    private void addMeal(String name,String formula,  Integer mealOfTheDay, Integer preparationTime, Date date, RealmList<Product> products) {
        MealService mealService = new MealService();
        mealService.createMeal(name, formula, mealOfTheDay, preparationTime, date, products);
    }

    private void addWorkout(String name, int time, Date date, boolean finished, RealmList<Exercise> exercises) {
        WorkoutService workoutService = new WorkoutService();
        workoutService.createWorkout(name, time, date, finished, exercises);

    }
}
