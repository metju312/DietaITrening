package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.SetService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.listAdapter.SetListAdapter;
import com.applications.matthew.dietaitrening.view.listAdapter.SetModifyListAdapter;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmResults;

public class ProceedWorkoutPreviewActivity extends BackActivity {

    private Workout actualWorkout;
    private Exercise actualExercise;
    private Spinner spinner;
    private ListView listView;
    private RealmResults<Set> setResults;
    private final List<String> setList = new ArrayList<>();
    private RealmResults<Exercise> exerciseResults;
    private SetModifyListAdapter adapter;
    private TextView timeTextView;
    private LinearLayout startStopWorkoutLayout;
    private ImageView imageView;

    private Calendar calendar;
    private long startTime = 0L;
    private long timeInMilliseconds = 0L;
    private long pauseTime = 0L;
    private long updatedTime = 0L;
    private boolean ifTimerStart = true;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proceed_preview);
        customizeToolbar();
        customizeActualWorkout();
        customizeActivityTitle(actualWorkout.getName());
        customizeSpinner();
        customizeListView();
        customizeStartStopWorkoutLayout();
        customizeImageView();
        customizeTimeTextView();
    }

    private void customizeImageView() {
        imageView = (ImageView) findViewById(R.id.imageView);
    }

    private void customizeStartStopWorkoutLayout() {
        startStopWorkoutLayout = (LinearLayout) findViewById(R.id.startStopWorkoutLayout);
        startStopWorkoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedWorkout();
            }
        });
    }

    private void customizeTimeTextView() {
        timeTextView = (TextView) findViewById(R.id.timeTextView);
    }

    private void customizeListView() {
        listView = (ListView) findViewById(R.id.listView);
        SetService setService = new SetService();
        setResults = setService.findAllWithExercise(actualExercise);
        adapter = new SetModifyListAdapter(this.getLayoutInflater(), setResults);
        listView.setAdapter(adapter);
    }

    private void customizeSetListAsStrings() {
        setList.clear();
        for (Set set : setResults) {
            setList.add(set.getNumberOfRepeatsToDo().toString() + "/" + set.getNumberOfRepeatsDone().toString() + " x " + set.getWeightToDo().toString() + "/" + set.getWeightDone().toString());
        }
    }

    private void customizeSpinner() {
        spinner = (Spinner) findViewById(R.id.spinner);
        ExerciseService exerciseService = new ExerciseService();
        exerciseResults = exerciseService.findAllWithWorkout(actualWorkout);
        if (exerciseResults.size() != 0) {
            actualExercise = exerciseResults.get(0);
        }
        List<String> list = new ArrayList<>();
        for (Exercise result : exerciseResults) {
            list.add(result.getName());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setSetResults(position);
                customizeListView();
                adapter.updateListView(setResults);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSetResults(int exercisePosition) {
        actualExercise = exerciseResults.get(exercisePosition);
        SetService setService = new SetService();
        setResults = setService.findAllWithExercise(actualExercise);
        customizeSetListAsStrings();
    }

    private void customizeActualWorkout() {
        WorkoutService workoutService = new WorkoutService();
        actualWorkout = workoutService.findById(getIntent().getLongExtra("actualItemId", 0));
    }

    public void proceedWorkout() {
        if (ifTimerStart) {
            calendar = Calendar.getInstance();
            startStopWorkoutLayout.setBackground(getDrawable(R.color.colorPrimary));
            imageView.setImageDrawable(getDrawable(R.drawable.ic_stop));

            startTime = SystemClock.uptimeMillis();
            handler.postDelayed(updateTimer, 0);
            ifTimerStart = false;
        } else {
            pauseTime += timeInMilliseconds;
            handler.removeCallbacks(updateTimer);
            ifTimerStart = true;

            setupResults();
            finish();
        }
    }

    private void setupResults() {
        WorkoutService workoutService = new WorkoutService();
        workoutService.setFinished(actualWorkout, true);
        int minutes = 0;
        minutes += calendar.get(Calendar.MINUTE);
        minutes += calendar.get(Calendar.HOUR_OF_DAY) * 60;
        workoutService.setTime(actualWorkout, minutes);

        Intent data = new Intent();
        setResult(RESULT_OK, data);
    }

    public Runnable updateTimer = new Runnable() {
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = pauseTime + timeInMilliseconds;

            DateFormat formatter = new SimpleDateFormat("H:mm:ss.S");
            calendar.setTimeInMillis(updatedTime);


            timeTextView.setText(formatter.format(calendar.getTime()));
            handler.postDelayed(this, 100);
        }
    };
}
