package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.SetService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;

public class ExerciseAddExistingActivity extends BackActivity {
    private ListView listView;
    private EditText editText;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exercise_add_existing_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.select_exercise_type));
        customizeListView();
        customizeEditText();
    }

    private void customizeEditText() {
        editText = (EditText) findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void customizeListView() {
        listView = (ListView) findViewById(R.id.listView);
        ExerciseService exerciseService = new ExerciseService();
        final RealmResults<Exercise> results = exerciseService.findAllDistinct("name").sort("name");
        List<String> list = new ArrayList<>();
        for (Exercise result : results) {
            list.add(result.getName());
        }
        adapter = new ArrayAdapter<>(this, R.layout.list_item_one_string, list);
        listView.setAdapter(adapter);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addExercise((String) parent.getItemAtPosition(position));
                setupResults();
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(this, ExerciseAddActivity.class);
                intent.putExtra("actualItemId", getIntent().getLongExtra("actualItemId", 0));
                startActivityForResult(intent, 1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupResults() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            setupResults();
            finish();
        }
    }

    private void addExercise(String name) {
        WorkoutService workoutService = new WorkoutService();
        ExerciseService exerciseService = new ExerciseService();
        SetService setService = new SetService();
        Exercise exerciseToCopy = exerciseService.findNewest(name);
        RealmList<Set> sets = new RealmList<>();
        for (Set setToCopy : exerciseToCopy.getSets()) {
            Set set = setService.createSet(setToCopy.getNumberOfRepeatsToDo(), setToCopy.getNumberOfRepeatsDone(), setToCopy.getWeightToDo(), setToCopy.getWeightDone());
            sets.add(set);
        }
        Workout workout = workoutService.findById(getIntent().getLongExtra("actualItemId", 0));
        Exercise exercise = exerciseService.createExercise(exerciseToCopy.getName(), exerciseToCopy.getTime(), workout.getStartDate(),  workout.getExercises().size() + 1, sets);
        workoutService.addExerciseToWorkout(exercise, workout);
    }
}
