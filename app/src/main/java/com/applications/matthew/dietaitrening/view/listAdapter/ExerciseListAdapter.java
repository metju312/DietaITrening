package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.view.activity.ExercisePreviewActivity;

import io.realm.RealmResults;

public class ExerciseListAdapter extends AbstractListAdapter<Exercise, ExercisePreviewActivity> {

    public ExerciseListAdapter(LayoutInflater inflater, RealmResults<Exercise> objects) {
        super(ExercisePreviewActivity.class, inflater, objects, false);
    }

    @Override
    protected String getPrimaryTextViewText(Exercise exercise) {
        return exercise.getOrder().toString() + ". " + exercise.getName();
    }

    @Override
    protected String getSecondaryTextViewText(Exercise exercise) {
        ExerciseService exerciseService = new ExerciseService();
        return exerciseService.getNumberRepeatsString(exercise);
    }

    @Override
    protected int getXmlLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected int getPrimaryTextViewId() {
        return R.id.itemPrimaryTextView;
    }

    @Override
    protected int getSecondaryTextViewId() {
        return R.id.itemSecondaryTextView;
    }

    @Override
    protected int getPopupMenuId() {
        return R.menu.popup_menu;
    }

    @Override
    protected int getListViewLayoutId() {
        return R.id.itemListViewLayout;
    }

    @Override
    public long getDatabaseItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected void delete(long id) {
        ExerciseService exerciseService = new ExerciseService();
        exerciseService.deleteById(id);
    }
}