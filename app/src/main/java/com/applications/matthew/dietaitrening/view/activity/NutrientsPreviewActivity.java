package com.applications.matthew.dietaitrening.view.activity;

import android.os.Bundle;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class NutrientsPreviewActivity extends BackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nutrients_preview);
        customizeToolbar();
    }
}
