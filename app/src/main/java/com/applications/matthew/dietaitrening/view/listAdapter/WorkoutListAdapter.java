package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.utils.Format;
import com.applications.matthew.dietaitrening.view.activity.WorkoutPreviewActivity;

import io.realm.RealmResults;

public class WorkoutListAdapter extends AbstractListAdapter<Workout, WorkoutPreviewActivity> {
    Format dateFormat = new Format(getInflater().getContext());

    public WorkoutListAdapter(LayoutInflater inflater, RealmResults<Workout> objects) {
        super(WorkoutPreviewActivity.class, inflater, objects, false);
    }

    @Override
    protected String getPrimaryTextViewText(Workout workout) {
        return workout.getName();
    }

    @Override
    protected String getSecondaryTextViewText(Workout workout) {
        return dateFormat.getHours(workout.getStartDate(), workout.getTime());
    }

    @Override
    protected int getXmlLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected int getPrimaryTextViewId() {
        return R.id.itemPrimaryTextView;
    }

    @Override
    protected int getSecondaryTextViewId() {
        return R.id.itemSecondaryTextView;
    }

    @Override
    protected int getPopupMenuId() {
        return R.menu.popup_menu;
    }

    @Override
    protected int getListViewLayoutId() {
        return R.id.itemListViewLayout;
    }

    @Override
    protected long getDatabaseItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected void delete(long id) {
        WorkoutService workoutService = new WorkoutService();
        workoutService.deleteById(id);
    }
}