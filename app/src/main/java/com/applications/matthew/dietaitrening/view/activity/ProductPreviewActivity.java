package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.services.NutrientsService;
import com.applications.matthew.dietaitrening.model.services.ProductService;
import com.applications.matthew.dietaitrening.view.listAdapter.NutrientsListAdapter;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

public class ProductPreviewActivity extends BackActivity {
    private Product actualProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_preview);
        customizeToolbar();
        customizeActivityTitle();
        customizeListView();
        customizeFloatingActionMenu();
    }

    private void customizeListView() {
        NutrientsService nutrientsService = new NutrientsService();
        NutrientsListAdapter nutrientsListAdapter = new NutrientsListAdapter(this.getLayoutInflater(), nutrientsService.findAllWithProduct(actualProduct).sort("id"));
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(nutrientsListAdapter);
    }

    private void customizeActivityTitle() {
        Intent intent = getIntent();
        long id = intent.getLongExtra("actualItemId", 0);
        ProductService productService = new ProductService();
        actualProduct = productService.findById(id);
        super.setTitle(actualProduct.getName());
    }

    private void customizeFloatingActionMenu() {
        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.floatingActionsMenu);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                startAddNutrientsPreviewActivity();
                floatingActionsMenu.collapse();
            }

            @Override
            public void onMenuCollapsed() {

            }
        });
    }

    private void startAddNutrientsPreviewActivity() {
        Intent intent = new Intent(this, NutrientsAddPreviewActivity.class);
        intent.putExtra("actualItemId", actualProduct.getId());
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            Toast.makeText(this, getString(R.string.onAddNutrientsToast), Toast.LENGTH_SHORT).show();
            customizeListView();
        }
    }
}
