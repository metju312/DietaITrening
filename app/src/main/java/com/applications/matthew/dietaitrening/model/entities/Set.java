package com.applications.matthew.dietaitrening.model.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Set extends RealmObject {
    @PrimaryKey
    private Long id;
    private Integer numberOfRepeatsToDo;
    private Integer numberOfRepeatsDone;
    private Double weightToDo;
    private Double weightDone;

    public Set() {
    }

    public Set(Long id, Integer numberOfRepeatsToDo, Integer numberOfRepeatsDone, Double weightToDo, Double weightDone) {
        this.id = id;
        this.numberOfRepeatsToDo = numberOfRepeatsToDo;
        this.numberOfRepeatsDone = numberOfRepeatsDone;
        this.weightToDo = weightToDo;
        this.weightDone = weightDone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberOfRepeatsToDo() {
        return numberOfRepeatsToDo;
    }

    public void setNumberOfRepeatsToDo(Integer numberOfRepeatsToDo) {
        this.numberOfRepeatsToDo = numberOfRepeatsToDo;
    }

    public Integer getNumberOfRepeatsDone() {
        return numberOfRepeatsDone;
    }

    public void setNumberOfRepeatsDone(Integer numberOfRepeatsDone) {
        this.numberOfRepeatsDone = numberOfRepeatsDone;
    }

    public Double getWeightToDo() {
        return weightToDo;
    }

    public void setWeightToDo(Double weightToDo) {
        this.weightToDo = weightToDo;
    }

    public Double getWeightDone() {
        return weightDone;
    }

    public void setWeightDone(Double weightDone) {
        this.weightDone = weightDone;
    }
}