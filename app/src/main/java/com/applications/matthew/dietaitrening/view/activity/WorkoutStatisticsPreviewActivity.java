package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class WorkoutStatisticsPreviewActivity extends BackActivity {
    private Workout actualWorkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workout_statistics_preview);
        customizeToolbar();
        customizeActivityTitle();
    }

    private void customizeActivityTitle() {
        Intent intent = getIntent();
        long workoutId = intent.getLongExtra("actualItemId", 0);
        WorkoutService workoutService = new WorkoutService();
        actualWorkout = workoutService.findById(workoutId);
        setTitle(actualWorkout.getName());
    }
}
