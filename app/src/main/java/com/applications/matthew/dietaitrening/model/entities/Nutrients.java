package com.applications.matthew.dietaitrening.model.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Nutrients extends RealmObject {

    @PrimaryKey
    private Long id;
    private Integer carbon;
    private Integer fat;
    private Integer protein;


    public Nutrients() {
    }

    public Nutrients(Long id, Integer carbon, Integer fat, Integer protein) {
        this.id = id;
        this.carbon = carbon;
        this.fat = fat;
        this.protein = protein;
    }

    public Long getId() {
        return id;
    }

    public Integer getCarbon() {
        return carbon;
    }

    public Integer getFat() {
        return fat;
    }

    public Integer getProtein() {
        return protein;
    }


}
