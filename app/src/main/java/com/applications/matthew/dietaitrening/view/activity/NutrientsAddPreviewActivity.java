package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Nutrients;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.services.NutrientsService;
import com.applications.matthew.dietaitrening.model.services.ProductService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import io.realm.RealmList;

public class NutrientsAddPreviewActivity extends BackActivity {

    private EditText proteinEditText;
    private EditText carbonEditText;
    private EditText fatEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nurients_add_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addProduct));
        customizeComponents();
    }

    private void customizeComponents() {
        proteinEditText = (EditText) findViewById(R.id.proteinEditText);
        carbonEditText = (EditText) findViewById(R.id.carbonEditText);
        fatEditText = (EditText) findViewById(R.id.fatEditText);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                if (proteinEditText.getText().toString().matches("") || carbonEditText.getText().toString().matches("") || fatEditText.getText().toString().matches("")) {
                    Toast.makeText(this, getString(R.string.nutrients_value_is_missing), Toast.LENGTH_SHORT).show();
                } else {
                    addNutrients();
                    setupResults();
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addNutrients() {
        ProductService productService = new ProductService();
        NutrientsService nutrientsService = new NutrientsService();


        Integer carbon = Integer.parseInt(carbonEditText.getText().toString());
        Integer fat = Integer.parseInt(fatEditText.getText().toString());
        Integer protein = Integer.parseInt(proteinEditText.getText().toString());


        Product product = productService.findById(getIntent().getLongExtra("actualItemId", 0));
        Nutrients nutrients = nutrientsService.createNutrients(carbon, fat, protein);
        productService.addNutrientsToProduct(nutrients, product);
    }

    private void setupResults() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);
    }
}
