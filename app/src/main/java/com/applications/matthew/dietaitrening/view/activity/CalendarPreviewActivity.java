package com.applications.matthew.dietaitrening.view.activity;

import android.os.Bundle;
import android.widget.CalendarView;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class CalendarPreviewActivity extends BackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.calendar));
        customizeCalendar();

    }

    private void customizeCalendar() {
        CalendarView  calendarView = (CalendarView ) findViewById(R.id.calendarView);
    }
}
