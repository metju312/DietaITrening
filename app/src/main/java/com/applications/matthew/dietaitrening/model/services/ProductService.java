package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.entities.Nutrients;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import io.realm.RealmList;

public class ProductService extends GenericServiceImpl<Product> {
    public ProductService() {
        super(Product.class);
    }

    public Product createProduct(String name, Integer calorific, RealmList<Nutrients> nutrientses) {
        return create(new Product(getNextId(), name, calorific, nutrientses));
    }

    public RealmList<Product> findAllWithMeal(Meal meal) {
        MealService mealService = new MealService();
        RealmList<Product> results = mealService.findById(meal.getId()).getProducts();
        return results;
    }

    public void addNutrientsToProduct(Nutrients nutrients, Product product) {
        getRealm().beginTransaction();
        product.getNutrientses().add(nutrients);
        getRealm().commitTransaction();
    }
}
