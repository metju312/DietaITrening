package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import java.io.File;
import java.util.Calendar;

public class ResultsAddPreviewActivity extends BackActivity {

    static final int CAMERA_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results_add_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addResults));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
//                if(exerciseNameEditText.getText().toString().matches("")){
//                    Toast.makeText(this, getString(R.string.exercise_name_is_missing),Toast.LENGTH_SHORT).show();
//                }else{
//                    addExercise();
//                    setupResults();
//                    finish();
//                }
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onImageViewClick(View v){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getFile();
        //cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(cameraIntent,CAMERA_REQUEST);
    }

    private File getFile(){
        File homePath = this.getFilesDir();
        File folder = new File(homePath.getAbsolutePath()+"/"+getString(R.string.app_folder_name));

        if(!folder.exists()){
            folder.mkdir();
        }

        Calendar calendar = Calendar.getInstance();
        File imageFile = new File(folder,"camera_image.jpg");
        return imageFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File homePath = this.getFilesDir();
        String path = homePath.getAbsolutePath()+"/"+getString(R.string.app_folder_name)+"/camera_image.jpg";
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        //imageView.setImageDrawable(Drawable.createFromPath(path));


        Bundle extras = data.getExtras();
        Bitmap photo = (Bitmap) extras.get("data");
        imageView.setImageBitmap(photo);
    }
}
