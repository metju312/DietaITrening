package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.Nutrients;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import io.realm.RealmResults;

public class NutrientsService extends GenericServiceImpl<Nutrients> {

    public NutrientsService() {
        super(Nutrients.class);
    }

    public Nutrients createNutrients(Integer carbon, Integer fat, Integer protein) {
        return create(new Nutrients(getNextId(), carbon, fat, protein));
    }

    public RealmResults<Nutrients> findAllWithProduct(Product product) {
        ProductService productService = new ProductService();
        RealmResults<Nutrients> results = productService.findById(product.getId()).getNutrientses().where().findAll().sort("id");
        return results;
    }
}
