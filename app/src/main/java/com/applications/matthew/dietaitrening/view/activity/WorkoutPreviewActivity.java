package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.listAdapter.ExerciseListAdapter;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

public class WorkoutPreviewActivity extends BackActivity {
    private Workout actualWorkout;
    private ExerciseListAdapter exerciseListAdapter;
    private static int ADD_EXERCISE_DIALOG_ID = 1;
    private static int WORKOUT_FINISHED_DIALOG_ID = 2;
    private static int SHOW_STATISTICS_DIALOG_ID = 3;
    private LinearLayout showStatisticsLayout;
    private TextView workoutStartRepeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workout_preview);
        customizeToolbar();
        customizeActivityTitle();
        customizeListView();
        customizeFloatingActionMenu();
        customizeWorkoutStartRepeatTextView();
        customizeShowStatisticsLayout();
    }

    private void customizeWorkoutStartRepeatTextView() {
        workoutStartRepeat = (TextView) findViewById(R.id.workoutStartRepeat);
    }

    private void customizeShowStatisticsLayout() {
        showStatisticsLayout = (LinearLayout) findViewById(R.id.showStatisticsLayout);
        showStatisticsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWorkoutStatisticsPreviewActivity();
            }
        });
        setStatisticsLayoutVisible();
    }

    private void setStatisticsLayoutVisible() {
        if (actualWorkout.isFinished()) {
            showStatisticsLayout.setVisibility(View.VISIBLE);
            workoutStartRepeat.setText(getString(R.string.workout_repeat));
        } else {
            showStatisticsLayout.setVisibility(View.GONE);
            workoutStartRepeat.setText(getString(R.string.workout_start));
        }
    }

    private void startWorkoutStatisticsPreviewActivity() {
        Intent intent = new Intent(this, WorkoutStatisticsPreviewActivity.class);
        intent.putExtra("actualItemId", actualWorkout.getId());
        startActivityForResult(intent, SHOW_STATISTICS_DIALOG_ID);
    }

    private void customizeFloatingActionMenu() {
        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.floatingActionsMenu);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                startAddExerciseActivity();
                floatingActionsMenu.collapse();
            }

            @Override
            public void onMenuCollapsed() {

            }
        });
    }

    private void customizeListView() {
        ExerciseService exerciseService = new ExerciseService();
        exerciseListAdapter = new ExerciseListAdapter(this.getLayoutInflater(), exerciseService.findAllWithWorkoutOrderByOrder(actualWorkout));
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(exerciseListAdapter);
    }

    private void customizeActivityTitle() {
        Intent intent = getIntent();
        long workoutId = intent.getLongExtra("actualItemId", 0);
        WorkoutService workoutService = new WorkoutService();
        actualWorkout = workoutService.findById(workoutId);
        setTitle(actualWorkout.getName());
    }

    private void startAddExerciseActivity() {
        Intent intent = new Intent(this, ExerciseAddExistingActivity.class);
        intent.putExtra("actualItemId", actualWorkout.getId());
        startActivityForResult(intent, ADD_EXERCISE_DIALOG_ID);
    }

    public void proceedWorkout(View v) {
        Intent intent = new Intent(this, ProceedWorkoutPreviewActivity.class);
        intent.putExtra("actualItemId", actualWorkout.getId());
        startActivityForResult(intent, WORKOUT_FINISHED_DIALOG_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == ADD_EXERCISE_DIALOG_ID) {
            ExerciseService exerciseService = new ExerciseService();
            exerciseListAdapter.updateListView(exerciseService.findAllWithWorkoutOrderByOrder(actualWorkout));
            customizeListView();
            Toast.makeText(this, getString(R.string.onAddExerciseToast), Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_OK && requestCode == WORKOUT_FINISHED_DIALOG_ID) {
            Toast.makeText(this, getString(R.string.onWorkoutFinishedToast) + " " +actualWorkout.getTime().toString() + " " + getString(R.string.minutes), Toast.LENGTH_LONG).show();
            setStatisticsLayoutVisible();
            customizeListView();
        }
    }
}
