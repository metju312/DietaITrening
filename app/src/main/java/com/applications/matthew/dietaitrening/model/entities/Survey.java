package com.applications.matthew.dietaitrening.model.entities;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Survey extends RealmObject {

    @PrimaryKey
    private Long id;
    private Double weight;
    private Date date;

    public Survey() {
    }

    public Survey(Long id, Double weight, Date date) {
        this.id = id;
        this.weight = weight;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
