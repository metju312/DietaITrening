package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import io.realm.RealmList;

public class ExerciseAddActivity extends BackActivity {

    private EditText exerciseDurationEditText;
    private EditText exerciseNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exercise_add_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addExercise));
        customizeExerciseNameEditText();
        customizeExerciseDurationEditText();
    }

    private void customizeExerciseDurationEditText() {
        exerciseDurationEditText = (EditText) findViewById(R.id.exerciseDurationEditText);
    }

    private void customizeExerciseNameEditText() {
        exerciseNameEditText = (EditText) findViewById(R.id.exerciseNameEditText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                if (exerciseNameEditText.getText().toString().matches("")) {
                    Toast.makeText(this, getString(R.string.exercise_name_is_missing), Toast.LENGTH_SHORT).show();
                } else {
                    addExercise();
                    setupResults();
                    finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addExercise() {
        WorkoutService workoutService = new WorkoutService();
        ExerciseService exerciseService = new ExerciseService();
        int duration;
        if (exerciseDurationEditText.getText().toString().matches("")) {
            duration = 0;
        } else {
            duration = Integer.parseInt(exerciseDurationEditText.getText().toString());
        }
        Workout workout = workoutService.findById(getIntent().getLongExtra("actualItemId", 0));
        Exercise exercise = exerciseService.createExercise(exerciseNameEditText.getText().toString(), duration, workout.getStartDate(), workout.getExercises().size() + 1,new RealmList<Set>());
        workoutService.addExerciseToWorkout(exercise, workout);
    }

    private void setupResults() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);
    }
}
