package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.User;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import java.util.Calendar;
import java.util.Date;

public class UserService extends GenericServiceImpl<User> {

    public UserService() {
        super(User.class);
    }

    public User createUser(String name, Double weight, Double weightGoal, Integer growth, Integer caloricGoal, Integer lowestWeight, Integer highestWeight, Date birthdayDate, Date joinDate, String picturePath) {
        return create(new User(getNextId(), name, weight, weightGoal, growth, caloricGoal, lowestWeight, highestWeight, birthdayDate, joinDate, picturePath));
    }

    public User findCurrentUser() {
        return findById(1L);
    }

    public void savePicturePath(String picturePath) {
        User user = findCurrentUser();
        getRealm().beginTransaction();
        user.setPicturePath(picturePath);
        getRealm().commitTransaction();
    }

    public int getUserAge(User user){
        Calendar dob = Calendar.getInstance();
        dob.setTime(user.getBirthdayDate());
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR))
            age--;
        return age;
    }
}
