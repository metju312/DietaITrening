package com.applications.matthew.dietaitrening.model.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject {

    @PrimaryKey
    private Long id;
    private String name;
    private Integer calorific;
    private RealmList<Nutrients> nutrientses;

    public Product() {
    }

    public Product(Long id, String name, Integer calorific, RealmList<Nutrients> nutrientses) {
        this.id = id;
        this.name = name;
        this.calorific = calorific;
        this.nutrientses = nutrientses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCalorific() {
        return calorific;
    }

    public void setCalorific(Integer calorific) {
        this.calorific = calorific;
    }

    public RealmList<Nutrients> getNutrientses() {
        return nutrientses;
    }

    public void setNutrientses(RealmList<Nutrients> nutrientses) {
        this.nutrientses = nutrientses;
    }
}
