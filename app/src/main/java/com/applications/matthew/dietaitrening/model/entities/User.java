package com.applications.matthew.dietaitrening.model.entities;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {

    @PrimaryKey
    private Long id;
    private String name;
    private Double weight;
    private Double weightGoal;
    private Integer growth;
    private Integer caloricGoal;
    private Integer lowestWeight;
    private Integer highestWeight;
    private Date birthdayDate;
    private Date joinDate;
    private String picturePath;

    public User() {
    }

    public User(Long id, String name, Double weight, Double weightGoal, Integer growth, Integer caloricGoal, Integer lowestWeight, Integer highestWeight, Date birthdayDate, Date joinDate, String picturePath) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.weightGoal = weightGoal;
        this.growth = growth;
        this.caloricGoal = caloricGoal;
        this.lowestWeight = lowestWeight;
        this.highestWeight = highestWeight;
        this.birthdayDate = birthdayDate;
        this.joinDate = joinDate;
        this.picturePath = picturePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getWeightGoal() {
        return weightGoal;
    }

    public void setWeightGoal(Double weightGoal) {
        this.weightGoal = weightGoal;
    }

    public Integer getGrowth() {
        return growth;
    }

    public void setGrowth(Integer growth) {
        this.growth = growth;
    }

    public Integer getCaloricGoal() {
        return caloricGoal;
    }

    public void setCaloricGoal(Integer caloricGoal) {
        this.caloricGoal = caloricGoal;
    }

    public Integer getLowestWeight() {
        return lowestWeight;
    }

    public void setLowestWeight(Integer lowestWeight) {
        this.lowestWeight = lowestWeight;
    }

    public Integer getHighestWeight() {
        return highestWeight;
    }

    public void setHighestWeight(Integer highestWeight) {
        this.highestWeight = highestWeight;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }
}
