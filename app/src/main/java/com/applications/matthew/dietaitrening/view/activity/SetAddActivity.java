package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.SetService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class SetAddActivity extends BackActivity {

    private EditText numberOfRepeatsEditText;
    private EditText numberOfRepeatsDoneEditText;
    private EditText weightEditText;
    private EditText weightDoneEditText;
    private LinearLayout layoutToHide;
    private Switch setDoneSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_add_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addSet));
        customizeComponents();
    }

    private void customizeComponents() {
        numberOfRepeatsEditText = (EditText) findViewById(R.id.numberOfRepeatsEditText);
        numberOfRepeatsEditText.setText("" + "1");
        numberOfRepeatsDoneEditText = (EditText) findViewById(R.id.numberOfRepeatsDoneEditText);
        numberOfRepeatsDoneEditText.setText("" + "0");
        weightEditText = (EditText) findViewById(R.id.weightEditText);
        weightEditText.setText("" + "1");
        weightDoneEditText = (EditText) findViewById(R.id.weightDoneEditText);
        weightDoneEditText.setText("" + "0");
        layoutToHide = (LinearLayout) findViewById(R.id.layoutToHide);
        layoutToHide.setVisibility(View.GONE);
        setDoneSwitch = (Switch) findViewById(R.id.setDoneSwitch);
        setDoneSwitch.setChecked(false);
        setDoneSwitch.setText(getString(R.string.no));
        setDoneSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(setDoneSwitch.isChecked()){
                    layoutToHide.setVisibility(View.VISIBLE);
                    setDoneSwitch.setText(getString(R.string.yes));
                }else{
                    layoutToHide.setVisibility(View.GONE);
                    setDoneSwitch.setText(getString(R.string.no));
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                if(numberOfRepeatsEditText.getText().toString().matches("")){
                    Toast.makeText(this, getString(R.string.exercise_name_is_missing),Toast.LENGTH_SHORT).show();
                }else{
                    try{
                        addSet();
                        setupResults();
                        finish();
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(this,getString(R.string.incompleteData), Toast.LENGTH_SHORT);
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addSet() {
        ExerciseService exerciseService = new ExerciseService();
        SetService setService = new SetService();

        int numberOfRepeats = 0;
        numberOfRepeats = Integer.parseInt(numberOfRepeatsEditText.getText().toString());

        int numberOfRepeatsDone = 0;
        numberOfRepeats = Integer.parseInt(numberOfRepeatsEditText.getText().toString());

        double weight = 0d;
        weight = Double.parseDouble(weightEditText.getText().toString());

        double weightDone = 0d;
        weightDone = Double.parseDouble(weightDoneEditText.getText().toString());

        if(!setDoneSwitch.isChecked()){
            weightDone = weight;
            numberOfRepeatsDone = 0;
        }else{
            numberOfRepeatsDone = Integer.parseInt(numberOfRepeatsDoneEditText.getText().toString());
            weightDone = Double.parseDouble(weightDoneEditText.getText().toString());
        }

        Exercise exercise = exerciseService.findById(getIntent().getLongExtra("actualItemId", 0));
        Set set = setService.createSet(numberOfRepeats, numberOfRepeatsDone,weight, weightDone);
        exerciseService.addSetToExercise(set, exercise);
    }

    private void setupResults() {
        Intent data = new Intent();
        data.putExtra("key1", "value1");
        data.putExtra("key2", "value2");
        setResult(RESULT_OK, data);
        finish();
    }
}
