package com.applications.matthew.dietaitrening.view.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Survey;
import com.applications.matthew.dietaitrening.model.services.SurveyService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmResults;

public class StatisticsPreviewActivity extends BackActivity {
    private GraphView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.statistics));
        customizeFirstGraph();
    }

    private void customizeFirstGraph() {
        graph = (GraphView) findViewById(R.id.graph);
        customizeGraph();
    }

    private void customizeGraph() {
        Calendar calendar = Calendar.getInstance();
        Log.v("pointsss calendar get", calendar.getTime().toString());
        calendar.add(Calendar.DATE, 9);
        Log.v("pointsss calendar +9 ", calendar.getTime().toString());
        SurveyService surveyService = new SurveyService();
        RealmResults<Survey> realmList = surveyService.findWeekResults(calendar);

//        Date d1 = calendar.getTime();
//        calendar.add(Calendar.DATE, 1);
//        Date d2 = calendar.getTime();
//        calendar.add(Calendar.DATE, 1);
//        Date d3 = calendar.getTime();

        RealmResults<Survey> realmListAll = surveyService.findAll();
        for (Survey survey: realmListAll) {
            Log.v("pointsss all", survey.getDate().toString());
        }

        for (Survey survey: realmList) {
            Log.v("pointsss not", survey.getDate().toString());
        }


        Log.v("pointsss", "first");

        DataPoint[] points = new DataPoint[realmList.size()];
        for (int i = 0; i < points.length; i++) {
            Survey survey = realmList.get(i);
            points[i] = new DataPoint(survey.getDate(),survey.getWeight());
            Log.v("pointsss", realmList.get(i).getDate().toString());
        }


        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(points);

        series.setTitle("Random Curve 1");
        series.setColor(Color.GREEN);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setThickness(8);

        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                makeToast(dataPoint);
            }
        });

        graph.addSeries(series);

        DateFormat dateFormat = new SimpleDateFormat("dd.MM");
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this,dateFormat));
        graph.getGridLabelRenderer().setNumHorizontalLabels(4);

        graph.getViewport().setMinX(realmList.first().getDate().getTime());
        graph.getViewport().setMaxX(realmList.last().getDate().getTime());

        graph.getGridLabelRenderer().setHumanRounding(true);

        graph.getViewport().setYAxisBoundsManual(false);
        graph.getViewport().setXAxisBoundsManual(false);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
    }

    private void makeToast(DataPointInterface dataPoint) {
        Toast.makeText(this, "Series1: On Data Point clicked: "+dataPoint, Toast.LENGTH_SHORT).show();
    }
}
