package com.applications.matthew.dietaitrening.model.generic;


import android.util.Log;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

public abstract class GenericServiceImpl<T extends RealmModel> implements GenericService<T> {
    private Class<T> type;
    private Realm realm;

    public GenericServiceImpl(Class<T> type) {
        super();
        realm = Realm.getDefaultInstance();
        this.type = type;
    }

    public Long getNextId() {
        try {
            return realm.where(type).max("id").longValue() + 1L;
        } catch (Exception ignored) {
            return 1L;
        }
    }

    @Override
    public T create(T t) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
        return t;
    }

    @Override
    public void deleteById(final Long id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<T> result = realm.where(type).equalTo("id", id).findAll();
                result.deleteAllFromRealm();
            }
        });
    }

    @Override
    public T findById(Long id) {
        return realm.where(type).equalTo("id", id).findFirst();
    }

    @Override
    public RealmResults<T> findAll() {
        return realm.where(type).findAll();
    }


    @Override
    public T update(T t) {
        create(t);
        return t;
    }

    @Override
    public RealmResults<T> getEmptyResults() {
        return realm.where(type).equalTo("id", 0).findAll();
    }

    @Override
    public Realm getRealm() {
        return realm;
    }

    @Override
    public void logItems() {
        RealmResults<T> results = findAll();
        for (T t : results) {
            Log.v("DB: " + type.toString(), t.toString());
        }
    }

    @Override
    public RealmResults<T> findAllDistinct(String field) {
        return realm.where(type).distinct(field);
    }
}
