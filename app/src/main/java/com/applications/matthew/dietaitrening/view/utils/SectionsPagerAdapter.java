package com.applications.matthew.dietaitrening.view.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.entities.Workout;

import io.realm.RealmResults;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private PlaceholderFragment placeholderFragment;

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        placeholderFragment = PlaceholderFragment.newInstance(position + 1);
        return placeholderFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Dieta";
            case 1:
                return "Trening";
        }
        return null;
    }

    public void updateListView(RealmResults<Workout> results){
        placeholderFragment.updateListView(results);
    }

    public void updateMealListView(RealmResults<Meal> results) {
        placeholderFragment.updateMealListView(results);
    }
}