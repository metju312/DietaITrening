package com.applications.matthew.dietaitrening.view.utils;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.applications.matthew.dietaitrening.R;


public class BackActivity extends ActionBarActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void customizeActivityTitle(String title) {
        setTitle(title);
    }

    public void customizeBackButtonOnToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void customizeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        customizeBackButtonOnToolbar();
    }

    public void setContentViewWithLayout(int layoutId) {
        setContentView(layoutId);
    }
}
