package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.services.SetService;

import io.realm.RealmResults;

public class SetModifyListAdapter extends BaseAdapter {
    private class ViewHolder {
        ImageView addNumberOfRepeats;
        ImageView subtractNumberOfRepeats;
        ImageView addWeight;
        ImageView subtractWeight;
        TextView numberOfRepeatsDoneTextView;
        TextView numberOfRepeatsToDoTextView;
        TextView weightDoneTextView;
        TextView weightToDoTextView;
    }

    private RealmResults<Set> objects;
    private final LayoutInflater inflater;

    public SetModifyListAdapter(LayoutInflater inflater, RealmResults<Set> objects) {
        this.inflater = inflater;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.set_list_item, null);
            holder = customizeView(position, convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setTextViewsVariables(holder, position);
        return convertView;
    }

    private void setTextViewsVariables(ViewHolder holder, int position) {
        Set set = objects.get(position);
        holder.numberOfRepeatsDoneTextView.setText(set.getNumberOfRepeatsDone().toString());
        holder.numberOfRepeatsToDoTextView.setText("/" + set.getNumberOfRepeatsToDo().toString());
        holder.weightDoneTextView.setText(set.getWeightDone().toString());
        holder.weightToDoTextView.setText("/" + set.getWeightToDo().toString());
    }

    private ViewHolder customizeView(final int position, View convertView, ViewHolder holder) {
        holder = new ViewHolder();
        final Set set = objects.get(position);
        final SetService setService = new SetService();
        holder.addNumberOfRepeats = (ImageView) convertView.findViewById(R.id.addNumberOfRepeats);
        final ViewHolder finalHolder = holder;
        holder.addNumberOfRepeats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(set.getNumberOfRepeatsDone() >= 0){
                    setService.updateNumberOfRepeatsDone(set, set.getNumberOfRepeatsDone() + 1);
                    setTextViewsVariables(finalHolder, position);
                }
            }
        });
        holder.subtractNumberOfRepeats = (ImageView) convertView.findViewById(R.id.subtractNumberOfRepeats);
        holder.subtractNumberOfRepeats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(set.getNumberOfRepeatsDone() > 0){
                    setService.updateNumberOfRepeatsDone(set, set.getNumberOfRepeatsDone() - 1);
                    setTextViewsVariables(finalHolder, position);
                }
            }
        });
        holder.addWeight = (ImageView) convertView.findViewById(R.id.addWeight);
        holder.addWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(set.getWeightDone() >= 0){
                    setService.updateWeightDone(set, set.getWeightDone() + 0.5);
                    setTextViewsVariables(finalHolder, position);
                }
            }
        });
        holder.subtractWeight = (ImageView) convertView.findViewById(R.id.subtractWeight);
        holder.subtractWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(set.getWeightDone() > 0){
                    setService.updateWeightDone(set, set.getWeightDone() - 0.5);
                    setTextViewsVariables(finalHolder, position);
                }
            }
        });
        holder.numberOfRepeatsDoneTextView = (TextView) convertView.findViewById(R.id.numberOfRepeatsDoneTextView);
        holder.weightDoneTextView = (TextView) convertView.findViewById(R.id.weightDoneTextView);
        holder.numberOfRepeatsToDoTextView = (TextView) convertView.findViewById(R.id.numberOfRepeatsToDoTextView);
        holder.weightToDoTextView = (TextView) convertView.findViewById(R.id.weightToDoTextView);

        return holder;
    }

    private int getXmlLayoutId() {
        return 0;
    }

    public void updateListView(RealmResults<Set> objectsToUpdate) {
        objects = objectsToUpdate;
        notifyDataSetChanged();
    }
}