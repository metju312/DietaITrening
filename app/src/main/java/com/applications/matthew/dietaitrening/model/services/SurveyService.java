package com.applications.matthew.dietaitrening.model.services;

import android.util.Log;

import com.applications.matthew.dietaitrening.model.entities.Survey;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmResults;

public class SurveyService extends GenericServiceImpl<Survey> {

    public SurveyService() {
        super(Survey.class);
    }

    public Survey createSurvey(Double weight, Date date) {
        return create(new Survey(getNextId(), weight, date));
    }

    public RealmResults<Survey> findWeekResults(Calendar calendarLastDay) {
        Calendar calendarFirstDay = Calendar.getInstance();
        calendarFirstDay.set(Calendar.YEAR, calendarLastDay.get(Calendar.YEAR));
        calendarFirstDay.set(Calendar.MONTH, calendarLastDay.get(Calendar.MONTH));
        calendarFirstDay.set(Calendar.DAY_OF_MONTH, calendarLastDay.get(Calendar.DAY_OF_MONTH));
        calendarFirstDay.add(Calendar.DAY_OF_MONTH, -7);
        Log.v("pointsss calendar last", calendarLastDay.getTime().toString());
        Log.v("pointsss calendar first", calendarFirstDay.getTime().toString());
        RealmResults<Survey> results = getRealm().where(Survey.class).between("date", calendarFirstDay.getTime(), calendarLastDay.getTime()).findAll();
        return results;
    }
}
