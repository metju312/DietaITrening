package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class CalculatorsPreviewActivity extends BackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculators_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.calculators));
    }

    public void onCalculatorBmiClick(View v){
        Intent intent = new Intent(this, CalculatorBmiActivity.class);
        startActivityForResult(intent, 1);
    }

    public void onCalculatorBmrClick(View v){
        Intent intent = new Intent(this, CalculatorBmrActivity.class);
        startActivityForResult(intent, 1);
    }
}
