package com.applications.matthew.dietaitrening.utils;

import android.content.Context;

import com.applications.matthew.dietaitrening.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Format {
    private Context context;

    public Format(Context context) {
        this.context = context;
    }

    public String widgetGetDayOfTheWeek(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        String day_short;
        switch (dayOfWeek) {
            case 1:
                day_short = context.getString(R.string.sunday_short);
                break;
            case 2:
                day_short = context.getString(R.string.monday_short);
                break;
            case 3:
                day_short = context.getString(R.string.tuesday_short);
                break;
            case 4:
                day_short = context.getString(R.string.wednesday_short);
                break;
            case 5:
                day_short = context.getString(R.string.thursday_short);
                break;
            case 6:
                day_short = context.getString(R.string.friday_short);
                break;
            case 7:
                day_short = context.getString(R.string.saturday_short);
                break;
            default:
                day_short = "Invalid day";
                break;
        }
        return day_short;
    }

    public String widgetGetDayOfTheWeek(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        String day_short;
        switch (dayOfWeek) {
            case 1:
                day_short = context.getString(R.string.sunday_short);
                break;
            case 2:
                day_short = context.getString(R.string.monday_short);
                break;
            case 3:
                day_short = context.getString(R.string.tuesday_short);
                break;
            case 4:
                day_short = context.getString(R.string.wednesday_short);
                break;
            case 5:
                day_short = context.getString(R.string.thursday_short);
                break;
            case 6:
                day_short = context.getString(R.string.friday_short);
                break;
            case 7:
                day_short = context.getString(R.string.saturday_short);
                break;
            default:
                day_short = "Invalid day";
                break;
        }
        return day_short + ".";
    }

    public String widgetGetDayAndMonth(Calendar calendar) {
        int month = calendar.get(Calendar.MONTH) + 1;
        String monthString;

        switch (month) {
            case 1:
                monthString = context.getString(R.string.january);
                break;
            case 2:
                monthString = context.getString(R.string.february);
                break;
            case 3:
                monthString = context.getString(R.string.march);
                break;
            case 4:
                monthString = context.getString(R.string.april);
                break;
            case 5:
                monthString = context.getString(R.string.may);
                break;
            case 6:
                monthString = context.getString(R.string.june);
                break;
            case 7:
                monthString = context.getString(R.string.july);
                break;
            case 8:
                monthString = context.getString(R.string.august);
                break;
            case 9:
                monthString = context.getString(R.string.september);
                break;
            case 10:
                monthString = context.getString(R.string.october);
                break;
            case 11:
                monthString = context.getString(R.string.november);
                break;
            case 12:
                monthString = context.getString(R.string.december);
                break;
            default:
                monthString = "Invalid month";
                break;
        }
        return calendar.get(Calendar.DAY_OF_MONTH) + " " + monthString;
    }

    public String getDay(Calendar calendar) {
        Calendar todayCalendar = Calendar.getInstance();
        int calendarDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int todayCalendarDayOfMonth = todayCalendar.get(Calendar.DAY_OF_MONTH);
        if (calendar.get(Calendar.YEAR) == todayCalendar.get(Calendar.YEAR)) {
            if (calendar.get(Calendar.MONTH) == todayCalendar.get(Calendar.MONTH)) {
                if (calendar.get(Calendar.DAY_OF_MONTH) == todayCalendar.get(Calendar.DAY_OF_MONTH)) {
                    return context.getString(R.string.today);
                } else if ((calendarDayOfMonth == todayCalendarDayOfMonth + 1) || (calendarDayOfMonth == 0 && todayCalendarDayOfMonth == 7)) {
                    return context.getString(R.string.tomorrow);
                }
            }
        }
        return getDayOfTheWeek(calendar);
    }

    public String getDate(Calendar calendar) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String strDate = sdf.format(calendar.getTime());
        return strDate;
    }

    public String getDayOfTheWeek(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        String day;
        switch (dayOfWeek) {
            case 1:
                day = context.getString(R.string.sunday);
                break;
            case 2:
                day = context.getString(R.string.monday);
                break;
            case 3:
                day = context.getString(R.string.tuesday);
                break;
            case 4:
                day = context.getString(R.string.wednesday);
                break;
            case 5:
                day = context.getString(R.string.thursday);
                break;
            case 6:
                day = context.getString(R.string.friday);
                break;
            case 7:
                day = context.getString(R.string.saturday);
                break;
            default:
                day = "Invalid day";
                break;
        }
        return day;
    }

    public String getHours(Date startDate, int minutesToAdd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String firstHour = sdf.format(calendar.getTime());
        calendar.add(Calendar.MINUTE, minutesToAdd);
        String secondHour = sdf.format(calendar.getTime());
        return firstHour + " - " + secondHour;
    }
}
