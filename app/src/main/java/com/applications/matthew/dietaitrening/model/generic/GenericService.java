package com.applications.matthew.dietaitrening.model.generic;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

interface GenericService<T extends RealmModel> {
    T create(T t);

    void deleteById(Long id);

    T findById(Long id);

    RealmResults<T> findAll();

    T update(T t);

    RealmResults<T> getEmptyResults();

    Realm getRealm();

    void logItems();

    RealmResults<T> findAllDistinct(String field);
}
