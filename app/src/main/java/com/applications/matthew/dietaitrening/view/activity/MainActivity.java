package com.applications.matthew.dietaitrening.view.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.utils.Format;
import com.applications.matthew.dietaitrening.view.utils.SectionsPagerAdapter;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private int datePickerDialogId = 0; //0 currentData //1 copy workouts //2 copy meals

    private final static int ADD_WORKOUT_DIALOG_ID = 1;
    private final static int ADD_MEAL_DIALOG_ID = 2;
    private final static int ADD_RESULTS_DIALOG_ID = 3;

    private Toolbar toolbar;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager viewPager;
    private TextView dayNameTextView;
    private TextView dateTextView;


    private Calendar selectedCalendar = Calendar.getInstance();
    private Format dateFormat = new Format(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        customizeToolbar();
        customizePager();
        customizeTabs();
        customizeDateLayout();
        customizeDateButtons();
        customizeDrawer();
        customizeNavigationView();
        setupWorkoutTypeDialogClickableLayouts();
    }

    private void setupWorkoutTypeDialogClickableLayouts() {

    }

    private void customizeNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void customizeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void customizeDateButtons() {
        dayNameTextView = (TextView) findViewById(R.id.dayNameTextView);
        dayNameTextView.setText(dateFormat.getDay(selectedCalendar));

        dateTextView = (TextView) findViewById(R.id.dateTextView);
        dateTextView.setText(dateFormat.getDate(selectedCalendar));

        ImageButton previousDayButton = (ImageButton) findViewById(R.id.previousDayButton);
        previousDayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDay(-1);
            }
        });
        ImageButton nextDayButton = (ImageButton) findViewById(R.id.nextDayButton);
        nextDayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDay(1);
            }
        });
    }

    private void customizeDateLayout() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.dateLayout);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialogId = 0;
                showDialog(datePickerDialogId);
            }
        });
    }

    private void customizeTabs() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void customizePager() {
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);
    }

    private void customizeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 0 || id == 1 || id == 2) {
            return new DatePickerDialog(this, datePickerListener, selectedCalendar.get(Calendar.YEAR), selectedCalendar.get(Calendar.MONTH), selectedCalendar.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            switch (datePickerDialogId) {
                case 0:
                    selectedCalendar.set(Calendar.YEAR, year);
                    selectedCalendar.set(Calendar.MONTH, monthOfYear);
                    selectedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateDay(0);
                    break;
                case 1:
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    copyWorkoutsOfTheDay(calendar);
                    break;
                case 2:
                    //TODO
                    break;
                default:
                    break;
            }
        }
    };

    private void copyWorkoutsOfTheDay(Calendar calendarFrom) {
        WorkoutService workoutService = new WorkoutService();
        workoutService.copyWorkoutsOfTheDay(calendarFrom, selectedCalendar);
        sectionsPagerAdapter.updateListView(workoutService.findAllWithStartDate(selectedCalendar));
        viewPager.setCurrentItem(1);
        datePickerDialogId = 0;
    }

    private void updateDay(int daysToUpdate) {
        selectedCalendar.add(Calendar.DATE, daysToUpdate);
        dayNameTextView.setText(dateFormat.getDay(selectedCalendar));
        dateTextView.setText(dateFormat.getDate(selectedCalendar));
        WorkoutService workoutService = new WorkoutService();
        sectionsPagerAdapter.updateListView(workoutService.findAllWithStartDate(selectedCalendar));


        MealService mealService = new MealService();
        sectionsPagerAdapter.updateMealListView(mealService.findAllWithDate(selectedCalendar));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            setupExitDialog();
        }
    }

    private void setupExitDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.exit_dialog);
        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.exitButton);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onExitClick();
            }
        });
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void onExitClick() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calendar) {
            Intent intent = new Intent(this, CalendarPreviewActivity.class);
            startActivityForResult(intent, 1);
        } else if (id == R.id.nav_results) {

        } else if (id == R.id.nav_statistics) {
            Intent intent = new Intent(this, StatisticsPreviewActivity.class);
            startActivityForResult(intent, 1);
        } else if (id == R.id.nav_calculators) {
            Intent intent = new Intent(this, CalculatorsPreviewActivity.class);
            startActivityForResult(intent, 1);
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(this, ProfilePreviewActivity.class);
            startActivityForResult(intent, 1);
        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_settings) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onAddWorkoutClick(View view) {
        hideFloatingActionMenu();
        startAddWorkoutActivity();
    }

    public void onCopyWorkoutsOfTheDayClick(View v) {
        datePickerDialogId = 1;
        showDialog(datePickerDialogId);
        hideFloatingActionMenu();
    }

    private void startAddWorkoutActivity() {
        Intent intent = new Intent(this, WorkoutAddActivity.class);
        intent.putExtra("calendar", selectedCalendar);
        startActivityForResult(intent, ADD_WORKOUT_DIALOG_ID);
    }

    public void onAddMealClick(View view) {
        hideFloatingActionMenu();
        startAddMealActivity();
    }

    private void startAddMealActivity() {
        Intent intent = new Intent(this, MealAddPreview.class);
        intent.putExtra("calendar", selectedCalendar);
        startActivityForResult(intent, ADD_MEAL_DIALOG_ID);
    }

    public void onAddResultsClick(View view) {
        startAddResultsActivity();
        hideFloatingActionMenu();
    }

    private void startAddResultsActivity() {
        Intent intent = new Intent(this, ResultsAddPreviewActivity.class);
        startActivityForResult(intent, ADD_RESULTS_DIALOG_ID);
    }

    private void hideFloatingActionMenu() {
        FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.floatingActionsMenu);
        floatingActionsMenu.collapse();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == ADD_WORKOUT_DIALOG_ID) {
            WorkoutService workoutService = new WorkoutService();
            sectionsPagerAdapter.updateListView(workoutService.findAllWithStartDate(selectedCalendar));
            Toast.makeText(this, getString(R.string.onAddWorkoutToast), Toast.LENGTH_SHORT).show();
            viewPager.setCurrentItem(1);
        } else if (resultCode == RESULT_OK && requestCode == ADD_MEAL_DIALOG_ID) {
            Toast.makeText(this, getString(R.string.onAddMealToast), Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_OK && requestCode == ADD_RESULTS_DIALOG_ID) {
            Toast.makeText(this, getString(R.string.onAddResultsToast), Toast.LENGTH_SHORT).show();
        }
    }
}