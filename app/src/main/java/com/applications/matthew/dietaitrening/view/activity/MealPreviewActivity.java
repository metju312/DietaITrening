package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.model.services.ProductService;
import com.applications.matthew.dietaitrening.view.listAdapter.ProductListAdapter;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

public class MealPreviewActivity extends BackActivity {
    private Meal actualMeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meal_preview);
        customizeToolbar();
        customizeActivityTitle();
        customizeListView();
        customizeFloatingActionMenu();
    }

    private void customizeListView() {
        ProductService productService = new ProductService();
        ProductListAdapter productListAdapter = new ProductListAdapter(this.getLayoutInflater(), productService.findAllWithMeal(actualMeal).sort("name"));
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(productListAdapter);
    }

    private void customizeActivityTitle() {
        Intent intent = getIntent();
        long id = intent.getLongExtra("actualItemId", 0);
        MealService mealService = new MealService();
        actualMeal = mealService.findById(id);
        super.setTitle(actualMeal.getName());
    }

    private void customizeFloatingActionMenu() {
        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.floatingActionsMenu);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                startAddProductPreviewActivity();
                floatingActionsMenu.collapse();
            }

            @Override
            public void onMenuCollapsed() {

            }
        });
    }

    private void startAddProductPreviewActivity() {
        Intent intent = new Intent(this, ProductAddPreviewActivity.class);
        intent.putExtra("actualItemId", actualMeal.getId());
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            Toast.makeText(this, getString(R.string.onAddProductToast), Toast.LENGTH_SHORT).show();
            customizeListView();
        }
    }
}
