package com.applications.matthew.dietaitrening.view.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.utils.Format;
import com.applications.matthew.dietaitrening.view.activity.MainActivity;
import com.applications.matthew.dietaitrening.view.activity.WorkoutPreviewActivity;
import com.applications.matthew.dietaitrening.view.widget.service.ExerciseListViewWidgetService;

import java.util.Calendar;

public class ExerciseWidgetProvider extends AppWidgetProvider {

    public static final String UPDATE_MEETING_ACTION = "android.appwidget.action.APPWIDGET_UPDATE";
    public static final String EXTRA_ITEM = "com.example.edockh.EXTRA_ITEM";

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        if (intent.getAction().equals(UPDATE_MEETING_ACTION)) {
            int appWidgetIds[] = mgr.getAppWidgetIds(new ComponentName(context, ExerciseWidgetProvider.class));
            Log.e("received", intent.getAction());
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.exerciseWidgetListView);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; ++i) {

            // Set up the intent that starts the ListViewService, which will
            // provide the views for this collection.
            Intent intent = new Intent(context, ExerciseListViewWidgetService.class);

            // Add the app widget ID to the intent extras.
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            // Instantiate the RemoteViews object for the app widget layout.
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_exercise);

            // Set up the RemoteViews object to use a RemoteViews adapter.
            // This adapter connects
            // to a RemoteViewsService  through the specified intent.
            // This is how you populate the data.
            rv.setRemoteAdapter(appWidgetIds[i], R.id.exerciseWidgetListView, intent);

            // Trigger listview item click
            Intent startActivityIntent = new Intent(context, WorkoutPreviewActivity.class);
            PendingIntent startActivityPendingIntent = PendingIntent.getActivity(context, 0, startActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setPendingIntentTemplate(R.id.exerciseWidgetListView, startActivityPendingIntent);

            // The empty view is displayed when the collection has no items.
            // It should be in the same layout used to instantiate the RemoteViews  object above.
            rv.setEmptyView(R.id.exerciseWidgetListView, R.id.emptyWidgetTextView);

            //
            // Do additional processing specific to this app widget...
            //

            setOnLayoutButtonClickedListener(context, rv);
            //rv.setInt(R.id.widget_workout,"setBackgroundColor",myColor);

            setWidgetDate(rv, context);

            appWidgetManager.updateAppWidget(appWidgetIds[i], rv);
        }
    }

    private void setWidgetDate(RemoteViews rv, Context context) {
        Format format = new Format(context);
        Calendar calendar = Calendar.getInstance();
        rv.setTextViewText(R.id.exerciseWidgetDayOfTheWeek, format.widgetGetDayOfTheWeek(calendar));
        rv.setTextViewText(R.id.exerciseWidgetDate, format.widgetGetDayAndMonth(calendar));

    }

    private void setOnLayoutButtonClickedListener(Context context, RemoteViews views) {
        Intent openApp = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, openApp, 0);
        views.setOnClickPendingIntent(R.id.exerciseWidgetLayoutButton, pendingIntent);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

