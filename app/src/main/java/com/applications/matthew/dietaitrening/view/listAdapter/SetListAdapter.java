package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.services.SetService;
import com.applications.matthew.dietaitrening.view.activity.SetPreviewActivity;

import io.realm.RealmResults;

public class SetListAdapter extends AbstractListAdapter<Set, SetPreviewActivity> {

    public SetListAdapter(LayoutInflater inflater, RealmResults<Set> objects) {
        super(SetPreviewActivity.class, inflater, objects, true);
    }

    @Override
    protected String getPrimaryTextViewText(Set set) {
        String text =
                "[" +
                        set.getNumberOfRepeatsDone() +
                        "/" + set.getNumberOfRepeatsToDo() +
                        " x " +
                        set.getWeightDone() +
                        "/" + set.getWeightToDo() +
                        "] ";
        return text;
    }

    @Override
    protected String getSecondaryTextViewText(Set set) {
        return "";
    }

    @Override
    protected int getXmlLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected int getPrimaryTextViewId() {
        return R.id.itemPrimaryTextView;
    }

    @Override
    protected int getSecondaryTextViewId() {
        return R.id.itemSecondaryTextView;
    }

    @Override
    protected int getPopupMenuId() {
        return R.menu.popup_menu;
    }

    @Override
    protected int getListViewLayoutId() {
        return R.id.itemListViewLayout;
    }

    @Override
    public long getDatabaseItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected void delete(long id) {
        SetService setService = new SetService();
        setService.deleteById(id);
    }


}