package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmResults;

public class WorkoutService extends GenericServiceImpl<Workout> {

    public WorkoutService() {
        super(Workout.class);
    }

    public Workout createWorkout(String name, Integer time, Date startDate, boolean finished, RealmList<Exercise> exercises) {
        return create(new Workout(getNextId(), name, time, startDate, finished, exercises));
    }

    public RealmResults<Workout> findAllWithStartDate(Calendar calendarToSearch) {
        Calendar calendarMin = Calendar.getInstance();
        calendarMin.set(Calendar.YEAR, calendarToSearch.get(Calendar.YEAR));
        calendarMin.set(Calendar.MONTH, calendarToSearch.get(Calendar.MONTH));
        calendarMin.set(Calendar.DAY_OF_MONTH, calendarToSearch.get(Calendar.DAY_OF_MONTH));
        calendarMin.set(Calendar.HOUR_OF_DAY, 0);
        calendarMin.set(Calendar.MINUTE, 0);
        calendarMin.set(Calendar.SECOND, 0);
        calendarMin.set(Calendar.MILLISECOND, 0);

        Calendar calendarMax = Calendar.getInstance();
        calendarMax.set(Calendar.YEAR, calendarToSearch.get(Calendar.YEAR));
        calendarMax.set(Calendar.MONTH, calendarToSearch.get(Calendar.MONTH));
        calendarMax.set(Calendar.DAY_OF_MONTH, calendarToSearch.get(Calendar.DAY_OF_MONTH));
        calendarMax.set(Calendar.HOUR_OF_DAY, 23);
        calendarMax.set(Calendar.MINUTE, 59);
        calendarMax.set(Calendar.SECOND, 59);
        calendarMax.set(Calendar.MILLISECOND, 59);

        RealmResults<Workout> realmResults = getRealm().where(Workout.class).between("startDate", calendarMin.getTime(), calendarMax.getTime()).findAll().sort("startDate");

        return realmResults;
    }

    public void copyWorkoutsOfTheDay(Calendar calendarFrom, Calendar calendarTarget) {
        RealmResults<Workout> workouts = findAllWithStartDate(calendarFrom);
        for (Workout workout : workouts) {
            Calendar tmp = Calendar.getInstance();
            tmp.setTime(workout.getStartDate());
            calendarTarget.set(Calendar.HOUR_OF_DAY, tmp.get(Calendar.HOUR_OF_DAY));
            calendarTarget.set(Calendar.MINUTE, tmp.get(Calendar.MINUTE));

            copyWorkout(workout, calendarTarget.getTime());
        }

    }

    private void copyWorkout(Workout workoutToCopy, Date dateToCopy) {
        ExerciseService exerciseService = new ExerciseService();
        SetService setService = new SetService();
        RealmList<Exercise> exercises = new RealmList<>();
        for (Exercise exerciseToCopy : workoutToCopy.getExercises()) {
            RealmList<Set> sets = new RealmList<>();
            for (Set setToCopy : exerciseToCopy.getSets()) {
                Set set = setService.createSet(setToCopy.getNumberOfRepeatsToDo(), setToCopy.getNumberOfRepeatsDone(), setToCopy.getWeightToDo(), setToCopy.getWeightDone());
                sets.add(set);
            }
            Exercise exercise = exerciseService.createExercise(exerciseToCopy.getName(),exerciseToCopy.getTime(), exerciseToCopy.getDate(), exerciseToCopy.getOrder(), sets);
            exercises.add(exercise);
        }
        createWorkout(workoutToCopy.getName(), workoutToCopy.getTime(), dateToCopy, false, exercises);
    }

    public void addExerciseToWorkout(Exercise exercise, Workout workout) {
        getRealm().beginTransaction();
        workout.getExercises().add(exercise);
        getRealm().commitTransaction();
    }

    public void setFinished(Workout actualWorkout, boolean finished) {
        getRealm().beginTransaction();
        actualWorkout.setFinished(finished);
        getRealm().commitTransaction();
    }

    public void setTime(Workout actualWorkout, int minutes) {
        getRealm().beginTransaction();
        actualWorkout.setTime(minutes);
        getRealm().commitTransaction();
    }
}
