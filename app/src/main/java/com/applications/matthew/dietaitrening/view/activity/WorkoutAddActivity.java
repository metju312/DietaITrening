package com.applications.matthew.dietaitrening.view.activity;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.RealmList;

public class WorkoutAddActivity extends BackActivity {
    private int timePickerDialogId = 0;
    private Calendar timeCalendar;
    private TextView startTimeTextView;
    private EditText workoutNameEditText;
    private EditText workoutDurationEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workout_add_preview);
        customizeToolbar();
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addWorkout));
        customizeWorkoutNameEditText();
        customizeWorkoutDurationEditText();
        customizeStartTimeTextView();
    }

    private void customizeWorkoutDurationEditText() {
        workoutDurationEditText = (EditText) findViewById(R.id.workoutDurationEditText);
    }

    private void customizeWorkoutNameEditText() {
        workoutNameEditText = (EditText) findViewById(R.id.workoutNameEditText);
    }

    private void customizeStartTimeTextView() {
        startTimeTextView = (TextView) findViewById(R.id.startTimeEditText);
        timeCalendar = (Calendar) getIntent().getSerializableExtra("calendar");
        setStartTimeTextViewText();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                if(workoutNameEditText.getText().toString().matches("")){
                    Toast.makeText(this, getString(R.string.workout_name_is_missing),Toast.LENGTH_SHORT).show();
                }else{
                    addWorkout();
                    setupResults();
                    finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addWorkout() {
        WorkoutService workoutService = new WorkoutService();
        int duration;
        if(workoutDurationEditText.getText().toString().matches("")){
            duration = 0;
        }else{
            duration = Integer.parseInt(workoutDurationEditText.getText().toString());
        }
        workoutService.createWorkout(workoutNameEditText.getText().toString(),duration,timeCalendar.getTime(), false, new RealmList<Exercise>());
    }

    private void setupResults() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);
        finish();
    }

    public void onHourTextViewClick(View v){
        showDialog(timePickerDialogId);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 0) {
            return new TimePickerDialog(this, timePickerListener, timeCalendar.get(Calendar.HOUR_OF_DAY),timeCalendar.get(Calendar.MINUTE),true);
        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
            timeCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            timeCalendar.set(Calendar.MINUTE, minute);
            setStartTimeTextViewText();
        }
    };

    private void setStartTimeTextViewText() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String str = sdf.format(timeCalendar.getTime());
        startTimeTextView.setText(str);
    }
}
