package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.services.ExerciseService;
import com.applications.matthew.dietaitrening.model.services.SetService;
import com.applications.matthew.dietaitrening.view.listAdapter.SetListAdapter;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

public class ExercisePreviewActivity extends BackActivity {
    private Exercise actualExercise;
    private SetListAdapter setListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exercise_preview);
        customizeToolbar();
        customizeActivityTitle();
        customizeListView();
        customizeFloatingActionMenu();
    }

    private void customizeFloatingActionMenu() {
        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.floatingActionsMenu);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                startAddSetActivity();
                floatingActionsMenu.collapse();
            }

            @Override
            public void onMenuCollapsed() {

            }
        });
    }

    private void startAddSetActivity() {
        Intent intent = new Intent(this, SetAddActivity.class);
        intent.putExtra("actualItemId", actualExercise.getId());
        startActivityForResult(intent, 1);
    }

    private void customizeListView() {
        SetService setService = new SetService();
        setListAdapter = new SetListAdapter(this.getLayoutInflater(), setService.findAllWithExercise(actualExercise));
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(setListAdapter);
    }

    private void customizeActivityTitle() {
        Intent intent = getIntent();
        long id = intent.getLongExtra("actualItemId", 0);
        ExerciseService exerciseService = new ExerciseService();
        actualExercise = exerciseService.findById(id);
        setTitle(actualExercise.getName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            SetService setService = new SetService();
            setListAdapter.updateListView(setService.findAllWithExercise(actualExercise));
            Toast.makeText(this, getString(R.string.onAddSetToast),Toast.LENGTH_SHORT).show();
        }
    }
}
