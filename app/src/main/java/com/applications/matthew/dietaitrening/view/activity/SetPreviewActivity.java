package com.applications.matthew.dietaitrening.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class SetPreviewActivity extends BackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_preview);
        customizeToolbar();
    }
}
