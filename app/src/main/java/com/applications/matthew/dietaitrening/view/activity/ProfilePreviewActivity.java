package com.applications.matthew.dietaitrening.view.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.User;
import com.applications.matthew.dietaitrening.model.services.UserService;
import com.applications.matthew.dietaitrening.view.listAdapter.ProfileListAdapter;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;
import com.applications.matthew.dietaitrening.view.utils.ProfileRow;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProfilePreviewActivity extends BackActivity {

    private ImageView imageView;
    private static int RESULT_SELECT_IMAGE = 1;
    private static final int REQUEST_CODE = 0x11;
    static final int CAMERA_REQUEST = 2;

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.profile));
        customizeImageView();
        customizeListView();
    }

    private void customizeListView() {
        List<ProfileRow> profileRows = generateProfileRows();
        ProfileListAdapter listAdapter = new ProfileListAdapter(this, profileRows);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(listAdapter);

    }

    private List<ProfileRow> generateProfileRows() {
        UserService userService = new UserService();
        User user = userService.findCurrentUser();
        List<ProfileRow> list = new ArrayList<ProfileRow>();
        list.add(new ProfileRow(getString(R.string.weightGoal),user.getWeightGoal().toString()));
        list.add(new ProfileRow(getString(R.string.caloricGoal),user.getCaloricGoal().toString()));
        list.add(new ProfileRow(getString(R.string.lowestWeight),user.getLowestWeight().toString()));
        list.add(new ProfileRow(getString(R.string.highestWeight),user.getHighestWeight().toString()));
        list.add(new ProfileRow(getString(R.string.joinDate),user.getJoinDate().toString()));
        return list;
    }

    private void customizeImageView() {
        imageView = (ImageView) findViewById(R.id.imageView);
        UserService userService = new UserService();
        String path = userService.findCurrentUser().getPicturePath();
        if (!path.matches("")) {
            try {
                setImageView();
            } catch (Exception e) {

            }
        }
    }

    private void setImageView() {
        UserService userService = new UserService();
        imageView.setImageBitmap(BitmapFactory.decodeFile(userService.findCurrentUser().getPicturePath()));
    }

    public void onImageViewClick(View v) {
        verifyStoragePermissions();
        generatePhotoTypeDialog();
    }

    private void generatePhotoTypeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.profile_photo_source_dialog);
        LinearLayout takePhotoLayout = (LinearLayout) dialog.findViewById(R.id.takePhotoLayout);
        takePhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
                dialog.dismiss();
            }
        });
        LinearLayout selectPhotoLayout = (LinearLayout) dialog.findViewById(R.id.selectPhotoLayout);
        selectPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPhoto();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void selectPhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_SELECT_IMAGE);
    }

    private void takePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_SELECT_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            UserService userService = new UserService();
            userService.savePicturePath(picturePath);
            setImageView();
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            String filename = "tmp.jpg";
            File sd = Environment.getExternalStorageDirectory();
            File dest = new File(sd, filename);

            Bundle extras = data.getExtras();
            Bitmap photo = (Bitmap) extras.get("data");
            try {
                FileOutputStream out = new FileOutputStream(dest);
                photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            UserService userService = new UserService();
            userService.savePicturePath(dest.getAbsolutePath());
            setImageView();
        } else {
            Toast.makeText(this, getString(R.string.onChoosePhotoError), Toast.LENGTH_SHORT).show();
        }
    }

    public void verifyStoragePermissions() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_CODE
            );
        }
    }

    private Bitmap rotateImage(String photoLocation) {
        Bitmap bitmap = BitmapFactory.decodeFile(photoLocation);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(photoLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}
