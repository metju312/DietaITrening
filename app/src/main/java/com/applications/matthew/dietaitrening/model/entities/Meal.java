package com.applications.matthew.dietaitrening.model.entities;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Meal extends RealmObject {

    @PrimaryKey
    private Long id;
    private String name;
    private String formula;
    private Integer preparationTime;
    private Integer mealOfTheDay;
    private Date date;
    private RealmList<Product> products;

    public Meal() {
    }

    public Meal(Long id, String name, String formula, Integer preparationTime, Integer mealOfTheDay, Date date, RealmList<Product> products) {
        this.id = id;
        this.name = name;
        this.formula = formula;
        this.preparationTime = preparationTime;
        this.mealOfTheDay = mealOfTheDay;
        this.date = date;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getMealOfTheDay() {
        return mealOfTheDay;
    }

    public void setMealOfTheDay(Integer mealOfTheDay) {
        this.mealOfTheDay = mealOfTheDay;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RealmList<Product> getProducts() {
        return products;
    }

    public void setProducts(RealmList<Product> products) {
        this.products = products;
    }
}
