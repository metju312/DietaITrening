package com.applications.matthew.dietaitrening.model.entities;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Exercise extends RealmObject {
    @PrimaryKey
    private Long id;
    @Index
    private String name;
    private Integer time;
    private Date date;
    private Integer order;
    private RealmList<Set> sets;

    public Exercise() {
    }

    public Exercise(Long id, String name, Integer time, Date date, Integer order, RealmList<Set> sets) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.date = date;
        this.order = order;
        this.sets = sets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public RealmList<Set> getSets() {
        return sets;
    }

    public void setSets(RealmList<Set> sets) {
        this.sets = sets;
    }
}
