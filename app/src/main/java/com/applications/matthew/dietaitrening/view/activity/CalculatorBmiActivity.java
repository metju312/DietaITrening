package com.applications.matthew.dietaitrening.view.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

public class CalculatorBmiActivity extends BackActivity {

    private EditText bodyMassEditText;
    private EditText growthEditText;
    private TextView bmiTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_bmi_preview);
        customizeToolbar();
        setTitle(getString(R.string.bmi_index_calculator));
        customizeComponents();
    }

    private void customizeComponents() {
        bmiTextView = (TextView) findViewById(R.id.bmiTextView);

        bodyMassEditText = (EditText) findViewById(R.id.bodyMassEditText);
        bodyMassEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tryToCalculateBmi();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        growthEditText = (EditText) findViewById(R.id.growthBodyText);
        growthEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tryToCalculateBmi();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void tryToCalculateBmi() {
        String bodyMassText = bodyMassEditText.getText().toString();
        String growthMassText = growthEditText.getText().toString();

        if (bodyMassText.length() >= 2 && growthMassText.length() >= 3) {
            try{
                calculateBmi(Integer.parseInt(bodyMassEditText.getText().toString()), Integer.parseInt(growthEditText.getText().toString()));
            }catch (Exception e){
                Toast.makeText(this,"Wrong numbers",Toast.LENGTH_SHORT);
            }
        } else {
            bmiTextView.setText("0.0");
            bmiTextView.setTextColor(Color.GREEN);
        }
    }


    private void calculateBmi(int bodyMass, int growth) {
        Double bmi = bodyMass/(mFromCm(growth)*mFromCm(growth));
        if(bmi < 18.5 || bmi >= 25){
            bmiTextView.setTextColor(Color.RED);
        }else{
            bmiTextView.setTextColor(Color.GREEN);
        }
        bmiTextView.setText(String.format("%.2f", bmi));
    }

    private double mFromCm(int cm){
        return (double)cm/100;
    }
}
