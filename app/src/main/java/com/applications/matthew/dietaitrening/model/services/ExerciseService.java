package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class ExerciseService extends GenericServiceImpl<Exercise> {

    public ExerciseService() {
        super(Exercise.class);
    }

    public Exercise createExercise(String name, Integer time, Date date, Integer order, RealmList<Set> sets) {
        return create(new Exercise(getNextId(), name, time, date, order, sets));
    }

    public RealmResults<Exercise> findAllWithWorkout(Workout workout) {
        WorkoutService workoutService = new WorkoutService();
        RealmResults<Exercise> results = workoutService.findById(workout.getId()).getExercises().sort("name");
        return results;
    }

    public RealmResults<Exercise> findAllWithWorkoutOrderByOrder(Workout workout) {
        WorkoutService workoutService = new WorkoutService();
        RealmResults<Exercise> results = workoutService.findById(workout.getId()).getExercises().sort("order");
        return results;
    }

    public String getNumberRepeatsString(Exercise exercise) {
        String results = "";
        for (Set set : exercise.getSets()) {
            results +=
                "[" +
                set.getNumberOfRepeatsDone() +
                "/" + set.getNumberOfRepeatsToDo() +
                " x " +
                set.getWeightDone() +
                "/" + set.getWeightToDo() +
                "] ";
        }
        return results;
    }

    public void addSetToExercise(Set set, Exercise exercise) {
        getRealm().beginTransaction();
        exercise.getSets().add(set);
        getRealm().commitTransaction();
    }

    public RealmResults<Exercise> findAllWithName(String name) {
        return getRealm().where(Exercise.class).equalTo("name", name).findAll();
    }

    public Exercise findNewest(String name) {
        return getRealm()
                .where(Exercise.class).equalTo("name", name)
                .findAllSorted("date", Sort.DESCENDING)
                .first();
    }
}
