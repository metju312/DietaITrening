package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import java.util.Calendar;

import io.realm.RealmList;

public class MealAddPreview extends BackActivity {

    private EditText descriptionEditText;
    private EditText formulaEditText;
    private EditText preparationTimeEditText;
    private Calendar calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meal_add_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addMeal));
        customizeEditTexts();
        customizeDateFromIntent();

    }

    private void customizeDateFromIntent() {
        calendar = (Calendar) getIntent().getSerializableExtra("calendar");
    }

    private void customizeEditTexts() {
        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        formulaEditText = (EditText) findViewById(R.id.formulaEditText);
        preparationTimeEditText = (EditText) findViewById(R.id.preparationTimeEditText);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                if(descriptionEditText.getText().toString().matches("")){
                    Toast.makeText(this, getString(R.string.exercise_name_is_missing),Toast.LENGTH_SHORT).show();
                }else{
                    addMeal();
                    setupResults();
                    finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupResults() {
        Intent data = new Intent();
        data.putExtra("key1", "value1");
        data.putExtra("key2", "value2");
        setResult(RESULT_OK, data);
        finish();
    }

    private void addMeal() {
        MealService mealService = new MealService();
        mealService.createMeal(descriptionEditText.getText().toString(),formulaEditText.getText().toString(),1,Integer.parseInt(preparationTimeEditText.getText().toString()),calendar.getTime(),new RealmList<Product>());
    }
}
