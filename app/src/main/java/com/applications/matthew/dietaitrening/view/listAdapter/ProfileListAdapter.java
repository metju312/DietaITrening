package com.applications.matthew.dietaitrening.view.listAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.view.utils.ProfileRow;

import java.util.List;

public class ProfileListAdapter extends ArrayAdapter<ProfileRow> {

    public ProfileListAdapter(Context context, List<ProfileRow> objects) {
        super(context, R.layout.profile_item, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.profile_item, parent,false);

        String primaryText = getItem(position).getPrimary();
        String secondaryText = getItem(position).getSecondary();
        TextView primary = (TextView) customView.findViewById(R.id.primary);
        TextView secondary = (TextView) customView.findViewById(R.id.secondary);
        primary.setText(primaryText);
        secondary.setText(secondaryText);

        return customView;
    }
}
