package com.applications.matthew.dietaitrening.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.entities.Nutrients;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.model.services.ProductService;
import com.applications.matthew.dietaitrening.view.utils.BackActivity;

import io.realm.RealmList;

public class ProductAddPreviewActivity extends BackActivity {

    private EditText productNameEditText;
    private EditText productCalorificEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_add_preview);
        customizeToolbar();
        customizeActivityTitle(getString(R.string.addProduct));
        customizeProductCalorificEditText();
        customizeProductNameEditText();
    }

    private void customizeProductNameEditText() {
        productNameEditText = (EditText) findViewById(R.id.productNameEditText);
    }

    private void customizeProductCalorificEditText() {
        productCalorificEditText = (EditText) findViewById(R.id.productCalorificEditText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                if (productNameEditText.getText().toString().matches("")) {
                    Toast.makeText(this, getString(R.string.product_name_is_missing), Toast.LENGTH_SHORT).show();
                } else {
                    addProduct();
                    setupResults();
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addProduct() {
        MealService mealService = new MealService();
        ProductService productService = new ProductService();
        int calorific;
        if (productCalorificEditText.getText().toString().matches("")) {
            calorific = 0;
        } else {
            calorific = Integer.parseInt(productCalorificEditText.getText().toString());
        }
        Meal meal = mealService.findById(getIntent().getLongExtra("actualItemId", 0));
        Product product = productService.createProduct(productNameEditText.getText().toString(), calorific, new RealmList<Nutrients>());
        mealService.addProductToMeal(product, meal);
    }

    private void setupResults() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);
    }
}
