package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.view.activity.MealPreviewActivity;

import io.realm.RealmResults;

public class MealListAdapter extends AbstractListAdapter<Meal, MealPreviewActivity> {

    public MealListAdapter(LayoutInflater inflater, RealmResults<Meal> objects) {
        super(MealPreviewActivity.class, inflater, objects, false);
    }

    @Override
    protected String getPrimaryTextViewText(Meal meal) {
        return meal.getName();
    }

    @Override
    protected String getSecondaryTextViewText(Meal meal) {
        return meal.getPreparationTime().toString();
    }

    @Override
    protected int getXmlLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected int getPrimaryTextViewId() {
        return R.id.itemPrimaryTextView;
    }

    @Override
    protected int getSecondaryTextViewId() {
        return R.id.itemSecondaryTextView;
    }

    @Override
    protected int getPopupMenuId() {
        return R.menu.popup_menu;
    }

    @Override
    protected int getListViewLayoutId() {
        return R.id.itemListViewLayout;
    }

    @Override
    public long getDatabaseItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected void delete(long id) {
        MealService mealService = new MealService();
        mealService.deleteById(id);
    }
}