package com.applications.matthew.dietaitrening.view.listAdapter;

import android.view.LayoutInflater;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Nutrients;
import com.applications.matthew.dietaitrening.model.services.NutrientsService;
import com.applications.matthew.dietaitrening.view.activity.NutrientsPreviewActivity;

import io.realm.RealmResults;

public class NutrientsListAdapter extends AbstractListAdapter<Nutrients, NutrientsPreviewActivity> {

    public NutrientsListAdapter(LayoutInflater inflater, RealmResults<Nutrients> objects) {
        super(NutrientsPreviewActivity.class, inflater, objects, true);
    }

    @Override
    protected String getPrimaryTextViewText(Nutrients nutrients) {
        return "Białka: " + nutrients.getProtein().toString() + " / Węglowodany: " + nutrients.getCarbon().toString() + " / Tłuszcze: " + nutrients.getFat().toString();
    }

    @Override
    protected String getSecondaryTextViewText(Nutrients nutrients) {
        return nutrients.getCarbon().toString() + " / " + nutrients.getFat().toString() + " / " + nutrients.getProtein().toString();
    }

    @Override
    protected int getXmlLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected int getPrimaryTextViewId() {
        return R.id.itemPrimaryTextView;
    }

    @Override
    protected int getSecondaryTextViewId() {
        return R.id.itemSecondaryTextView;
    }

    @Override
    protected int getPopupMenuId() {
        return R.menu.popup_menu;
    }

    @Override
    protected int getListViewLayoutId() {
        return R.id.itemListViewLayout;
    }

    @Override
    public long getDatabaseItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected void delete(long id) {
        NutrientsService nutrientsService = new NutrientsService();
        nutrientsService.deleteById(id);
    }
}