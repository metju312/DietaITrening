package com.applications.matthew.dietaitrening.view.listAdapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.applications.matthew.dietaitrening.R;

import io.realm.RealmObject;
import io.realm.RealmResults;

abstract class AbstractListAdapter<T extends RealmObject, U> extends BaseAdapter {

    private class ViewHolder {
        TextView primaryTextView;
        TextView secondaryTextView;
        ImageButton popupMenuButton;
        LinearLayout linearLayout;
    }

    private Class<U> previewActivityType;
    private final LayoutInflater inflater;
    private RealmResults<T> objects;
    private boolean isLastList = false;

    AbstractListAdapter(Class<U> previewActivityType, LayoutInflater inflater, RealmResults<T> objects, boolean isLastList) {
        this.previewActivityType = previewActivityType;
        this.inflater = inflater;
        this.objects = objects;
        this.isLastList = isLastList;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public T getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(getXmlLayoutId(), null);
            holder = customizeView(position, convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.primaryTextView.setText(getPrimaryTextViewText(getItem(position)));
        holder.secondaryTextView.setText(getSecondaryTextViewText(getItem(position)));
        return convertView;
    }

    LayoutInflater getInflater() {
        return inflater;
    }

    public void setObjects(RealmResults<T> objects) {
        this.objects = objects;
    }

    protected abstract String getSecondaryTextViewText(T t);

    protected abstract String getPrimaryTextViewText(T t);

    protected abstract int getXmlLayoutId();

    protected abstract int getPrimaryTextViewId();

    protected abstract int getSecondaryTextViewId();

    protected abstract int getPopupMenuId();

    protected abstract int getListViewLayoutId();

    protected abstract long getDatabaseItemId(int position);

    private ViewHolder customizeView(final int position, View convertView, ViewHolder holder) {
        holder = new ViewHolder();
        holder.primaryTextView = (TextView) convertView.findViewById(getPrimaryTextViewId());
        holder.secondaryTextView = (TextView) convertView.findViewById(getSecondaryTextViewId());
        customizePopupMenuButton(position, convertView, holder);
        customizeClickableLayout(position, convertView, holder);
        return holder;
    }

    private void customizePopupMenuButton(final int position, View convertView, final ViewHolder holder) {
        holder.popupMenuButton = (ImageButton) convertView.findViewById(R.id.popupMenuButton);
        holder.popupMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.popupMenuButton, position);
            }
        });
    }

    private void customizeClickableLayout(final int position, View convertView, ViewHolder holder) {
        if(!isLastList){
            holder.linearLayout = (LinearLayout) convertView.findViewById(getListViewLayoutId());
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(inflater.getContext(), previewActivityType);
                    intent.putExtra("actualItemId", getDatabaseItemId(position));
                    inflater.getContext().startActivity(intent);
                }
            });
        }else{
            holder.secondaryTextView.setVisibility(View.GONE);
        }
    }

    private void showPopupMenu(ImageButton viewHolder, final int position) {
        PopupMenu popupMenu = new PopupMenu(viewHolder.getContext(), viewHolder);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(getPopupMenuId(), popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.delete:
                        //TODO
                        Log.v("PopupMenu delete", "costam" + position);
                        delete(getDatabaseItemId(position));
                        updateListView(objects);
                        return true;
                    case R.id.edit:
                        Log.v("PopupMenu edit", "costam" + position);
                        return true;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    protected abstract void delete(long id);

    public void updateListView(RealmResults<T> results) {
        setObjects(results);
        notifyDataSetChanged();
    }
}
