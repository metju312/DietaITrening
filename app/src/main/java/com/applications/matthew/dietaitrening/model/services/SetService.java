package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.Exercise;
import com.applications.matthew.dietaitrening.model.entities.Set;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import io.realm.RealmResults;

public class SetService extends GenericServiceImpl<Set> {

public SetService() {
        super(Set.class);
        }

public Set createSet(Integer numberOfRepeatsToDo, Integer numberOfRepeatsDone, Double weightToDo, Double weightDone) {
        return create(new Set(getNextId(), numberOfRepeatsToDo, numberOfRepeatsDone, weightToDo, weightDone));
        }

public RealmResults<Set> findAllWithExercise(Exercise exercise) {
        ExerciseService exerciseService = new ExerciseService();
        RealmResults<Set> results = exerciseService.findById(exercise.getId()).getSets().where().findAll().sort("id");
        return results;
        }

public void updateNumberOfRepeatsDone(Set set, int newNumberOfRepeatsDone) {
        getRealm().beginTransaction();
        set.setNumberOfRepeatsDone(newNumberOfRepeatsDone);
        getRealm().commitTransaction();
        }

public void updateWeightDone(Set set, double newWeightDone) {
        getRealm().beginTransaction();
        set.setWeightDone(newWeightDone);
        getRealm().commitTransaction();
        }
        }
