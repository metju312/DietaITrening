package com.applications.matthew.dietaitrening.view.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.applications.matthew.dietaitrening.R;
import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.entities.Workout;
import com.applications.matthew.dietaitrening.model.services.MealService;
import com.applications.matthew.dietaitrening.model.services.WorkoutService;
import com.applications.matthew.dietaitrening.view.listAdapter.MealListAdapter;
import com.applications.matthew.dietaitrening.view.listAdapter.WorkoutListAdapter;

import java.util.Calendar;

import io.realm.RealmResults;

public class PlaceholderFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private LayoutInflater inflater;
    private View mealRootView;
    private View workoutRootView;
    private WorkoutListAdapter workoutAdapter;
    private MealListAdapter mealAdapter;

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;

        WorkoutService workoutService = new WorkoutService();
        workoutAdapter = new WorkoutListAdapter(inflater, workoutService.findAllWithStartDate(Calendar.getInstance()));

        MealService mealService = new MealService();
        mealAdapter = new MealListAdapter(inflater, mealService.findAllWithDate(Calendar.getInstance()));




        mealRootView = inflater.inflate(R.layout.meal_fragment, container, false);
        workoutRootView = inflater.inflate(R.layout.workout_fragment, container, false);

        generateListViews(inflater);


        if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
            return mealRootView;
        } else {
            return workoutRootView;
        }
    }

    private void generateListViews(LayoutInflater inflater) {
        generateMealListView(mealRootView, inflater);
        generateWorkoutListView(workoutRootView, inflater);
    }

    private void generateWorkoutListView(View rootView, LayoutInflater inflater) {
        ListView listView = (ListView) rootView.findViewById(R.id.workoutListView);
        listView.setAdapter(workoutAdapter);

    }

    private void generateMealListView(View rootView, LayoutInflater inflater) {
        ListView listView = (ListView) rootView.findViewById(R.id.mealListView);
        listView.setAdapter(mealAdapter);
    }

    public void updateListView(RealmResults<Workout> results) {
        generateListViews(inflater);
        ListView listView = (ListView) workoutRootView.findViewById(R.id.workoutListView);
        listView.invalidateViews();
        listView.refreshDrawableState();
        workoutAdapter.updateListView(results);
    }

    public void updateMealListView(RealmResults<Meal> results) {
        generateListViews(inflater);
        ListView listView = (ListView) mealRootView.findViewById(R.id.mealListView);
        listView.invalidateViews();
        listView.refreshDrawableState();
        mealAdapter.updateListView(results);

        mealAdapter.notifyDataSetInvalidated();
        listView.invalidateViews();
    }
}
