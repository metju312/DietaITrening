package com.applications.matthew.dietaitrening.model.services;

import com.applications.matthew.dietaitrening.model.entities.Meal;
import com.applications.matthew.dietaitrening.model.entities.Product;
import com.applications.matthew.dietaitrening.model.generic.GenericServiceImpl;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmResults;

public class MealService extends GenericServiceImpl<Meal> {

    public MealService() {
        super(Meal.class);
    }

    public Meal createMeal(String name, String formula, Integer mealOfTheDay, Integer preparationTime, Date date, RealmList<Product> products) {
        return create(new Meal(getNextId(), name, formula, mealOfTheDay, preparationTime, date, products));
    }

    public RealmResults<Meal> findAllWithDate(Calendar calendarToSearch) {
        Calendar calendarMin = Calendar.getInstance();
        calendarMin.set(Calendar.YEAR, calendarToSearch.get(Calendar.YEAR));
        calendarMin.set(Calendar.MONTH, calendarToSearch.get(Calendar.MONTH));
        calendarMin.set(Calendar.DAY_OF_MONTH, calendarToSearch.get(Calendar.DAY_OF_MONTH));
        calendarMin.set(Calendar.HOUR_OF_DAY, 0);
        calendarMin.set(Calendar.MINUTE, 0);
        calendarMin.set(Calendar.SECOND, 0);
        calendarMin.set(Calendar.MILLISECOND, 0);

        Calendar calendarMax = Calendar.getInstance();
        calendarMax.set(Calendar.YEAR, calendarToSearch.get(Calendar.YEAR));
        calendarMax.set(Calendar.MONTH, calendarToSearch.get(Calendar.MONTH));
        calendarMax.set(Calendar.DAY_OF_MONTH, calendarToSearch.get(Calendar.DAY_OF_MONTH));
        calendarMax.set(Calendar.HOUR_OF_DAY, 23);
        calendarMax.set(Calendar.MINUTE, 59);
        calendarMax.set(Calendar.SECOND, 59);
        calendarMax.set(Calendar.MILLISECOND, 59);

        RealmResults<Meal> realmResults = getRealm().where(Meal.class).between("date", calendarMin.getTime(), calendarMax.getTime()).findAll().sort("date");

        return realmResults;
    }

    public void addProductToMeal(Product product, Meal meal) {
        getRealm().beginTransaction();
        meal.getProducts().add(product);
        getRealm().commitTransaction();
    }

}
